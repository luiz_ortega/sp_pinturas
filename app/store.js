import Vue from 'vue';
import Vuex from 'vuex';
import * as http from 'http';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pedidos: [],
    configuracao: {
        valor_metro_quadrado: 0,
        numero_de_andares: 0,
        numero_de_predios: 0,
        
        areaExterna_fator_multiplicador: 0,
        areaInterna_fator_multiplicador: 0,
        areaExtra_fator_multiplicador: 0,
        
        superficies: [],
        lista_servicos: [],
        lista_area_extra: [],
        lista_materiais: [],

    },
    pedido: {
        id_pedido: 1,
        valor_total: 0,
        orcamento: {
            orcamento_area_externa: {
                valor_total: 0,
                detalhes_valor: {
                    valor_pintura: 0,
                    valor_servicos: 0
                },
                metragem: 0,
                superficie: "",
                servicos: []
            },
            orcamento_area_interna: {
                valor_total: 0,
                detalhes_valor: {
                    valor_pintura: 0,
                    valor_servicos: 0
                },
                metragem: 0,
                superficie: "",
                servicos: []
            },
            orcamento_area_extra: {
                valor_total: 0,
                areas: []
            },
            materiais: []
        }
    },

    areaExtraTemp: {
        isEditExtra: false,
        isExtra: false,
        nome: "",
        valor_total: 0,
        detalhes_valor: {
            valor_pintura: 0,
            valor_servicos: 0
        },
        metragem: 0,
        superficie: "",
        servicos: []
    },

    areaInternaConfig: {
        isEditInterna: false,
        isInterna: false,
    },
  },
  getters: {
    orcamento_valor_total: state => {
        
        let list = [];

        state.pedido.orcamento.orcamento_area_extra.areas.forEach(el => { list.push(el.valor_total) });

        return parseFloat((state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura 
                         + parseFloat((state.pedido.orcamento.orcamento_area_externa.servicos.map(function(e){
                            return e.valor
                        }).reduce((sum, a) => sum + a,0).toFixed(2))) +
                        state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura 
                         + parseFloat((state.pedido.orcamento.orcamento_area_interna.servicos.map(function(e){
                            return e.valor
                        }).reduce((sum, a) => sum + a,0).toFixed(2))) +
                            
                            parseFloat((list.reduce((sum ,a) => sum + a,0)).toFixed(2))
                        
                        ).toFixed(2))
    },

/////// Configurações ///////////////////////////////////////////////////
    setar_superficies: state => {
        return state.configuracao.superficies;
    },
    setar_servicos: state => {
        return state.configuracao.lista_servicos;
    },
    setar_areas_extras: state => {
        return state.configuracao.lista_area_extra;
    },
    setar_materiais: state => {
        return state.configuracao.lista_materiais;
    },

/////// Area Externa ///////////////////////////////////////////////////

    orcamento_area_externa_valor: state => {
        let valor_pintura_externa = state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura;
        let valor_servicos_externa = state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos;
        
        return parseFloat((valor_pintura_externa + valor_servicos_externa).toFixed(2));
    },

    orcamento_area_externa_detalhes_valor_pintura: state => {
        return state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura;
    },

    orcamento_area_externa_detalhes_valor_servicos: state => {
        return parseFloat((state.pedido.orcamento.orcamento_area_externa.servicos.map(function(e){
            return e.valor
        }).reduce((sum, a) => sum + a,0).toFixed(2)));
    },

    orcamento_area_externa_servicos: state => {
        return state.pedido.orcamento.orcamento_area_externa.servicos;
    },




/////// Area Interna /////////////////////////////////////////////////// 
    orcamento_area_interna_valor: state => {
        let valor_pintura_interna = state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura; 
        let valor_servicos_interna = state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos; 

        return parseFloat((valor_pintura_interna + valor_servicos_interna).toFixed(2));
    },

    orcamento_area_interna_detalhes_valor_pintura: state => {
        return state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura;
    },

    orcamento_area_interna_detalhes_valor_servicos: state => {
        return parseFloat((state.pedido.orcamento.orcamento_area_interna.servicos.map(function(e){
            return e.valor
        }).reduce((sum, a) => sum + a,0).toFixed(2)));
    },

    orcamento_area_interna_servicos: state => {
        return state.pedido.orcamento.orcamento_area_interna.servicos;
    },

/////// Area Extras ///////////////////////////////////////////////////
    orcamento_area_extras_areas: state => {
        return state.pedido.orcamento.orcamento_area_extra.areas;
    },

    orcamento_area_extras_valor_total: state => {
        let list = [];
        state.pedido.orcamento.orcamento_area_extra.areas.forEach(el => { list.push(el.valor_total) });
        return parseFloat((list.reduce((sum ,a) => sum + a,0)).toFixed(2))
    },



////// TEMPORARY ADD EXTRA///////////////////////////////////////////////////// 
    orcamento_area_extra_valor: state => {
        let valor_pintura_extra = state.areaExtraTemp.detalhes_valor.valor_pintura;
        let valor_servicos_extra = state.areaExtraTemp.detalhes_valor.valor_servicos;
        
        return parseFloat((valor_pintura_extra + valor_servicos_extra).toFixed(2));
    },   

    orcamento_area_extra_modal_valor: state => {
        return state.areaExtraTemp.valor_total;
    },

    orcamento_area_extra_modal_nome: state => {
        return state.areaExtraTemp.nome;
    },

    orcamento_area_extra_modal_isEdit: state => {
        return state.areaExtraTemp.isEditExtra;
    },

    orcamento_area_extra_servicos: state => {
        return state.areaExtraTemp.servicos;
    },

    orcamento_area_extra_detalhes_valor_pintura: state => {
        return state.areaExtraTemp.detalhes_valor.valor_pintura;
    },

    orcamento_area_extra_detalhes_valor_servicos: state => {
        return parseFloat((state.areaExtraTemp.servicos.map(function(e){
            return e.valor
        }).reduce((sum, a) => sum + a,0).toFixed(2)));
    },

    orcamento_area_extra_is_extra: state => {
        return state.areaExtraTemp.isExtra;
    },


    ////// TEMPORARY ADD Interna///////////////////////////////////////////////////// 

    orcamento_area_interna_is_interna: state => {
        return state.areaInternaConfig.isInterna;
    },

////// MATERIAIS ///////

    set_materiais: state => {
        return state.pedido.orcamento.materiais;
    },

    
  },

  mutations: {
/////// Conf ///////////////////////////////////////////////////
AREA_EXTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR: (state, payload) => {
    state.configuracao.areaExterna_fator_multiplicador = payload;
},
AREA_INTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR: (state, payload) => {
    state.configuracao.areaInterna_fator_multiplicador = payload;
},
AREA_EXTRA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR: (state, payload) => {
    state.configuracao.areaExtra_fator_multiplicador = payload;
},

  
RECEBER_PEDIDOS: (state) => {
    http.getJSON("https://sppinturas.herokuapp.com/pedidos").then(
        result => { 
            state.pedidos = result.pedidos;
        },
        error => {
            console.log(error);
        }
    );
},

ENVIAR_ORCAMENTO: (state) => {
    state.pedido.valor_total = state.pedido.orcamento.orcamento_area_externa.valor_total +
                               state.pedido.orcamento.orcamento_area_interna.valor_total +
                               state.pedido.orcamento.orcamento_area_extra.valor_total
},

RECEBER_SUPERFICIES: (state) => {
    http.getJSON("https://sppinturas.herokuapp.com/superficies_disponiveis").then(
        result => { 
            state.configuracao.superficies = result.superficies_disponiveis;
        },
        error => {
            console.log(error);
        }
    );
},

RECEBER_SERVICOS: (state) => {
    let list = []
    http.getJSON("https://sppinturas.herokuapp.com/servicos_disponiveis").then(
        function(result) { 
            result.servicos_disponiveis.forEach(el => { list.push(el.descricao) });
            state.configuracao.lista_servicos = list
        },
        error => {
            console.log(error);
        }
    );
},

RECEBER_AREAS_EXTRAS: (state) => {
    let list = []
    http.getJSON("https://sppinturas.herokuapp.com/areas_extras_disponiveis").then(
        function(result) { 
            result.areas_extras_disponiveis.forEach(el => { list.push(el.nome) });
            state.configuracao.lista_area_extra = list
        },
        error => {
            console.log(error);
        }
    );
},

RECEBER_MATERIAIS: (state) => {
    let list = []
    http.getJSON("https://sppinturas.herokuapp.com/materiais_disponiveis").then(
        function(result) { 
            result.materiais_disponiveis.forEach(el => { list.push(el.descricao) });
            state.configuracao.lista_materiais = list
        },
        error => {
            console.log(error);
        }
    );
},   

SETAR_CONFIGURACOES_PEDIDO: (state, payload) => {
    state.configuracao.numero_de_andares = payload.numero_de_andares;
    state.configuracao.numero_de_predios = payload.numero_de_predios;
    state.pedido.id_pedido = payload.id_pedido;
},

SETAR_VALOR_METRO: (state) => {
    http.getJSON("https://sppinturas.herokuapp.com/valor_do_metro_quadrado").then(
        function(result) { 
            state.configuracao.valor_metro_quadrado  = result.valor_do_metro_quadrado.valor
        },
        error => {
            console.log(error);
        }
    );
},

ZERAR_PEDIDO: (state) => {
    state.pedido = {
        id_pedido: 1,
        valor_total: 0,
        orcamento: {
            orcamento_area_externa: {
                valor_total: 0,
                detalhes_valor: {
                    valor_pintura: 0,
                    valor_servicos: 0
                },
                metragem: 0,
                superficie: "",
                servicos: []
            },
            orcamento_area_interna: {
                valor_total: 0,
                detalhes_valor: {
                    valor_pintura: 0,
                    valor_servicos: 0
                },
                metragem: 0,
                superficie: "",
                servicos: []
            },
            orcamento_area_extra: {
                valor_total: 0,
                areas: []
            },
            materiais: []
        }
    }
},



/////// Area Externa/////////////////////////////////////////////
        AREA_EXTERNA_SETAR_VALOR_TOTAL: (state) => {
            state.pedido.orcamento.orcamento_area_externa.valor_total = parseFloat( 
              (  state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura + 
                state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos ).toFixed(2)
            );
        },

        // Detalhes
        AREA_EXTERNA_SETAR_DETALHES_VALOR_PINTURA: (state, payload) => {
            state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura = 
            parseFloat((payload * 
                        state.configuracao.valor_metro_quadrado *
                        state.configuracao.numero_de_andares *
                        state.configuracao.numero_de_predios *
                        state.configuracao.areaExterna_fator_multiplicador).toFixed(2));
        },
        AREA_EXTERNA_RECALCULAR_DETALHES_VALOR_PINTURA: (state) => {
            state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura = 
            parseFloat((state.pedido.orcamento.orcamento_area_externa.metragem * 
                        state.configuracao.valor_metro_quadrado *
                        state.configuracao.numero_de_andares *
                        state.configuracao.numero_de_predios *
                        state.configuracao.areaExterna_fator_multiplicador).toFixed(2));
        },

        AREA_EXTERNA_RECALCULAR_VALOR_SERVICOS: (state) => {
            state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos = parseFloat((state.pedido.orcamento.orcamento_area_externa.servicos.map(function(e){
                return e.valor
            }).reduce((sum, a) => sum + a,0).toFixed(2)))
        },


        AREA_EXTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO: (state, payload) => {
            let valorAtual = parseFloat((state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos).toFixed(2)); 
            let valorEntrada = parseFloat(payload.toFixed(2));
            let total = parseFloat((valorAtual + valorEntrada).toFixed(2));

            state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos = total;
        },

        AREA_EXTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO: (state, payload) => {
            let valorAtual = parseFloat((state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos).toFixed(2)); 
            let valorEntrada = parseFloat(payload.toFixed(2));
            let total = parseFloat((valorAtual - valorEntrada.toFixed(2)));
    
            state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos = total;
        },
        AREA_EXTERNA_ZERAR_DETALHES_VALOR_PINTURA: (state, payload) =>{
            state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura = 0;
        },
        AREA_EXTERNA_SETAR_SUPERFICIE: (state, payload) => {
            state.pedido.orcamento.orcamento_area_externa.superficie = payload
        },
        

        //Metragem
        AREA_EXTERNA_METRAGEM: (state, payload) => {
            state.pedido.orcamento.orcamento_area_externa.metragem = payload;
        },

        //Servicos
        AREA_EXTERNA_ADICIONAR_SERVICOS: (state, payload) => {
            state.pedido.orcamento.orcamento_area_externa.servicos.push(payload)
        },

        AREA_EXTERNA_REMOVER_SERVICOS: (state, payload) => {
            state.pedido.orcamento.orcamento_area_externa.servicos.splice(payload, 1)
        },

        AREA_EXTERNA_SALVAR_SERVICOS: (state, payload) => {
            state.pedido.orcamento.orcamento_area_externa.servicos.splice(payload.index, 1, payload.element)
        },



    /////// Area Interna ///////////////////////////////////////////////////
    AREA_INTERNA_SETAR_VALOR_TOTAL: (state) => {
        state.pedido.orcamento.orcamento_area_interna.valor_total = parseFloat( 
           ( state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura + 
            state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos ).toFixed(2)
        );
    },

    // Detalhes
    AREA_INTERNA_SETAR_DETALHES_VALOR_PINTURA: (state, payload) => {
        state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura = 
            parseFloat((payload * 
                        state.configuracao.valor_metro_quadrado *
                        state.configuracao.numero_de_andares *
                        state.configuracao.numero_de_predios *
                        state.configuracao.areaInterna_fator_multiplicador).toFixed(2)
            );
    },
    AREA_INTERNA_RECALCULAR_DETALHES_VALOR_PINTURA: (state) => {
        state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura = 
            parseFloat((
                        state.pedido.orcamento.orcamento_area_interna.metragem * 
                        state.configuracao.valor_metro_quadrado *
                        state.configuracao.numero_de_andares *
                        state.configuracao.numero_de_predios *
                        state.configuracao.areaInterna_fator_multiplicador).toFixed(2)
            );
    },

    AREA_INTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO: (state, payload) => {
        let valorAtual = parseFloat((state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos).toFixed(2)); 
        let valorEntrada = parseFloat(payload.toFixed(2));
        let total = parseFloat((valorAtual + valorEntrada).toFixed(2));

        state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos = total;
    },
    AREA_INTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO: (state, payload) => {
        let valorAtual = parseFloat((state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos).toFixed(2)); 
        let valorEntrada = parseFloat(payload.toFixed(2));
        let total = parseFloat((valorAtual - valorEntrada.toFixed(2)));

        state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos = total;
    },

    AREA_INTERNA_ZERAR_DETALHES_VALOR_PINTURA: (state, payload) =>{
        state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura = 0;
    },
    AREA_INTERNA_SETAR_SUPERFICIE: (state, payload) => {
        state.pedido.orcamento.orcamento_area_interna.superficie = payload
    },

//Metragem
    AREA_INTERNA_METRAGEM: (state, payload) => {
        state.pedido.orcamento.orcamento_area_interna.metragem = payload;
    },
//Servicos
    AREA_INTERNA_ADICIONAR_SERVICOS: (state, payload) => {
        state.pedido.orcamento.orcamento_area_interna.servicos.push(payload)
    },

    AREA_INTERNA_REMOVER_SERVICOS: (state, payload) => {
        state.pedido.orcamento.orcamento_area_interna.servicos.splice(payload, 1)
    },

    AREA_INTERNA_SALVAR_SERVICOS: (state, payload) => {
        state.pedido.orcamento.orcamento_area_interna.servicos.splice(payload.index, 1, payload.element)
    },    

    AREA_INTERNA_RECALCULAR_VALOR_SERVICOS: (state) => {
        state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos = parseFloat((state.pedido.orcamento.orcamento_area_interna.servicos.map(function(e){
            return e.valor
        }).reduce((sum, a) => sum + a,0).toFixed(2)))
    },



/////// Area Extra ///////////////////////////////////////////////////
    AREAS_EXTRAS_CALCULAR_VALOR_TOTAL: (state, payload) => {
        state.pedido.orcamento.orcamento_area_extra.valor_total = payload;
    },

    AREA_EXTRA_ADICIONAR_AREA: (state, payload) => {
        state.pedido.orcamento.orcamento_area_extra.areas.push(payload);
    },
    AREA_EXTRA_SALVAR_AREA: (state, payload) => {
        state.pedido.orcamento.orcamento_area_extra.areas.splice(payload.index, 1, payload.element)
    },

/////// Area Extra MODAL ///////////////////////////////////////////////////
    AREA_EXTRA_SETAR_NOME: (state, payload) => {
        state.areaExtraTemp.nome = payload;
    }, 
    AREA_EXTRA_ZERAR_NOME: (state) => {
        state.areaExtraTemp.nome = ''
    },
    AREA_EXTRA_METRAGEM: (state, payload) => {
        state.areaExtraTemp.metragem = payload;
    },
    AREA_EXTRA_ZERAR_DETALHES_VALOR_PINTURA: (state) => {
        state.areaExtraTemp.detalhes_valor.valor_pintura = 0;
    },
    AREA_EXTRA_RECALCULAR_DETALHES_VALOR_PINTURA: (state) => {
        state.areaExtraTemp.detalhes_valor.valor_pintura = 
            parseFloat((
                        state.areaExtraTemp.metragem * 
                        state.configuracao.valor_metro_quadrado *
                        state.configuracao.numero_de_andares *
                        state.configuracao.numero_de_predios *
                        state.configuracao.areaExtra_fator_multiplicador).toFixed(2)
            );
    },
    AREA_EXTRA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO: (state, payload) => {
        let valorAtual = parseFloat((state.areaExtraTemp.detalhes_valor.valor_servicos).toFixed(2)); 
        let valorEntrada = parseFloat(payload.toFixed(2));
        let total = parseFloat((valorAtual + valorEntrada).toFixed(2));

        state.areaExtraTemp.detalhes_valor.valor_servicos = total;
    },
    AREA_EXTRA_IS_EXTRA: (state) => {
        state.areaExtraTemp.isExtra = true;
    },
    AREA_EXTRA_ISNT_EXTRA: (state) => {
        state.areaExtraTemp.isExtra = false;
    },
    AREA_EXTRA_ADICIONAR_SERVICOS: (state, payload) => {
        state.areaExtraTemp.servicos.push(payload)
    },
    AREA_EXTRA_REMOVER_SERVICOS: (state, payload) => {
        state.areaExtraTemp.servicos.splice(payload, 1)
    },

    AREA_EXTRA_SALVAR_SERVICOS: (state, payload) => {
        state.areaExtraTemp.servicos.splice(payload.index, 1, payload.element)
    },
    AREA_EXTRA_RESETAR: (state) =>
        state.areaExtraTemp = {
            isEditExtra: false,
            isExtra: false,
            nome: "",
            valor_total: 0,
            detalhes_valor: {
                valor_pintura: 0,
                valor_servicos: 0
            },
            metragem: 0,
            superficie: "",
            servicos: []
        },
    AREA_EXTRA_ZERAR_SUPERFICIE: (state) => {
        state.configuracao.areaExtra_fator_multiplicador = 0
    },    
    AREA_EXTRA_SETAR_SUPERFICIE: (state, payload) => {
        state.areaExtraTemp.superficie = payload
    },
    AREA_EXTRA_SETAR_VALOR_TOTAL: (state, payload) => {
        state.areaExtraTemp.valor_total = payload
    },
    AREA_EXTRA_SETAR_MODAL: (state, payload) => {
        state.areaExtraTemp = payload
    },
    AREA_EXTRA_SALVAR_MODAL: (state, payload) => {
        state.pedido.orcamento.orcamento_area_extra.areas.splice(payload.index, 1, payload.element)
    },
    AREA_EXTRA_EXCLUIR_MODAL: (state, payload) => {
        state.pedido.orcamento.orcamento_area_extra.areas.splice(payload, 1)
    },
 
//////// Materiais ////////
    MATERIAIS_ADICIONAR: (state, payload) => {
        state.pedido.orcamento.materiais.push(payload)
    },  
    MATERIAS_REMOVER: (state, payload) => {
        state.pedido.orcamento.materiais.splice(payload, 1)
    },  
    MATERIAS_SALVAR: (state, payload) => {
        state.pedido.orcamento.materiais.splice(payload.index, 1, payload.element)
    },


  },




  actions: {
// Config
    AreaExterna_setar_fator_multiplicador(context, payload){
        context.commit( 'AREA_EXTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR', payload );
    },
    AreaInterna_setar_fator_multiplicador(context, payload){
        context.commit( 'AREA_INTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR', payload );
    },
    AreaExtra_setar_fator_multiplicador(context, payload){
        context.commit( 'AREA_EXTRA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR', payload );
    },

    receber_superficies(context) {
        context.commit( 'RECEBER_SUPERFICIES' )
    },

    receber_servicos(context) {
        context.commit( 'RECEBER_SERVICOS' )
    },

    receber_areas_extras(context) {
        context.commit( 'RECEBER_AREAS_EXTRAS' )
    },

    receber_materiais(context) {
        context.commit( 'RECEBER_MATERIAIS' )
    },

    receber_configuracoes_pedido(context,payload) {
        context.commit( 'SETAR_CONFIGURACOES_PEDIDO', payload )
    },

    receber_valor_metro(context) {
        context.commit( 'SETAR_VALOR_METRO' )
    },


    ///////  Pedidos ////////
    receber_pedidos(context) {
        context.commit( 'RECEBER_PEDIDOS' )
    },

    zerar_pedidos(context) {
        context.commit( 'ZERAR_PEDIDO' )
    },


/////// Area Externa ///////////////////////////////////////////////////
    AreaExterna_setar_valor_total(context) {
        context.commit( 'AREA_EXTERNA_SETAR_VALOR_TOTAL')
    },
    AreaExterna_recalcular_detalhes_valor_pintura(context){
        context.commit( 'AREA_EXTERNA_RECALCULAR_DETALHES_VALOR_PINTURA' );
    },
    AreaExterna_setar_metragem(context, payload) {
        context.commit( 'AREA_EXTERNA_METRAGEM', payload )
    },
    AreaExterna_setar_detalhes_valor_pintura(context, payload) {
        context.commit( 'AREA_EXTERNA_SETAR_DETALHES_VALOR_PINTURA', payload ) 
    },  
    AreaExterna_recalcular_valor_servico(context) {
        context.commit( 'AREA_EXTERNA_RECALCULAR_VALOR_SERVICOS' )
    },

    AreaExterna_adicionar_servico(context, payload) {
        context.commit( 'AREA_EXTERNA_ADICIONAR_SERVICOS', payload )
    },

    AreaExterna_remover_servico(context, payload) {
        context.commit( 'AREA_EXTERNA_REMOVER_SERVICOS', payload )
    },

    AreaExterna_salvar_servico(context, payload) {
        context.commit( 'AREA_EXTERNA_SALVAR_SERVICOS', payload )
    },

    AreaExterna_adicionar_valor_servicos(context, payload) {
        context.commit( 'AREA_EXTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload )
    },

    AreaExterna_remover_valor_servicos(context, payload) {
        context.commit( 'AREA_EXTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO', payload )
    },
    AreaExterna_zerar_valor_pintura(context, payload) {
        context.commit( 'AREA_EXTERNA_ZERAR_DETALHES_VALOR_PINTURA', payload )
    },
    AreaExterna_setar_superficie(context, payload) {
        context.commit( 'AREA_EXTERNA_SETAR_SUPERFICIE', payload )
    },


/////// Area Interna ///////////////////////////////////////////////////
    AreaInterna_setar_valor_total(context) {
        context.commit( 'AREA_INTERNA_SETAR_VALOR_TOTAL')
    },
    AreaInterna_recalcular_detalhes_valor_pintura(context){
        context.commit( 'AREA_INTERNA_RECALCULAR_DETALHES_VALOR_PINTURA' );
    },
    AreaInterna_setar_metragem(context, payload) {
        context.commit( 'AREA_INTERNA_METRAGEM', payload )
    },
    AreaInterna_setar_detalhes_valor_pintura(context, payload) {
        context.commit( 'AREA_INTERNA_SETAR_DETALHES_VALOR_PINTURA', payload ) 
    },  

    AreaInterna_adicionar_servico(context, payload) {
        context.commit( 'AREA_INTERNA_ADICIONAR_SERVICOS', payload )
    },

    AreaInterna_remover_servico(context, payload) {
        context.commit( 'AREA_INTERNA_REMOVER_SERVICOS', payload )
    },

    AreaInterna_salvar_servico(context, payload) {
        context.commit( 'AREA_INTERNA_SALVAR_SERVICOS', payload )
    },

    AreaInterna_adicionar_valor_servicos(context, payload) {
        context.commit( 'AREA_INTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload )
    },

    AreaInterna_remover_valor_servicos(context, payload) {
        context.commit( 'AREA_INTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO', payload )
    },
    AreaInterna_zerar_valor_pintura(context, payload) {
        context.commit( 'AREA_INTERNA_ZERAR_DETALHES_VALOR_PINTURA', payload )
    },
    AreaInterna_setar_superficie(context, payload) {
        context.commit( 'AREA_INTERNA_SETAR_SUPERFICIE', payload )
    },
    AreaInterna_recalcular_valor_servicos(context) {
        context.commit( 'AREA_INTERNA_RECALCULAR_VALOR_SERVICOS' )
    },



/////// Area Extra ///////////////////////////////////////////////////
   
    AreaExtra_adicionar_area(context, payload) {
        context.commit( 'AREA_EXTRA_ADICIONAR_AREA', payload )
    },
    AreaExtra_salvar_area(context, payload) {
        context.commit( 'AREA_EXTRA_SALVAR_AREA', payload )
    },

/////// Area Extra Modal ///////////////////////////////////////////////////

    AreaExtra_recalcular_detalhes_valor_pintura(context){
        context.commit( 'AREA_EXTRA_RECALCULAR_DETALHES_VALOR_PINTURA' );
    },
    
    AreaExtra_setar_nome(context, payload){
        context.commit( 'AREA_EXTRA_SETAR_NOME', payload )
    },
    AreaExtra_zerar_nome(context) {
        context.commit( 'AREA_EXTRA_ZERAR_NOME' )
    },

    AreaExtra_setar_metragem(context, payload) {
        context.commit( 'AREA_EXTRA_METRAGEM', payload )
    },

    AreaExtra_zerar_valor_pintura(context) {
        context.commit( 'AREA_EXTRA_ZERAR_DETALHES_VALOR_PINTURA')
    },

    AreaExtra_is_extra(context) {
        context.commit( 'AREA_EXTRA_IS_EXTRA' )
    },
    AreaExtra_isnt_extra(context) {
        context.commit( 'AREA_EXTRA_ISNT_EXTRA' )
    },
    AreaExtra_adicionar_servico(context, payload) {
        context.commit( 'AREA_EXTRA_ADICIONAR_SERVICOS', payload )
    },
    AreaExtra_adicionar_valor_servico(context, payload) {
        context.commit( 'AREA_EXTRA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload )
    },
    AreaExtra_remover_servico(context, payload) {
        context.commit( 'AREA_EXTRA_REMOVER_SERVICOS', payload )
    },

    AreaExtra_salvar_servico(context, payload) {
        context.commit( 'AREA_EXTRA_SALVAR_SERVICOS', payload )
    },

    AreaExtra_resetar(context) {
        context.commit( 'AREA_EXTRA_RESETAR' )
    },

    AreaExtra_setar_superficie(context, payload) {
        context.commit( 'AREA_EXTRA_SETAR_SUPERFICIE', payload )
    },
    AreaExtra_zerar_superficie(context) {
        context.commit( 'AREA_EXTRA_ZERAR_SUPERFICIE' )
    },

    AreaExtra_setar_valor_total(context, payload){
        context.commit( 'AREA_EXTRA_SETAR_VALOR_TOTAL', payload )
    },
    AreaExtra_setar_area(context, payload){
        context.commit( 'AREA_EXTRA_SETAR_MODAL', payload )
    },
    AreaExtra_salvar_area(context, payload){
        context.commit( 'AREA_EXTRA_SALVAR_MODAL', payload )
    },
    AreaExtra_excluir_area(context, payload){
        context.commit( 'AREA_EXTRA_EXCLUIR_MODAL', payload )
    },
 
///////  Materiais  //////    
    materiais_adicionar(context, payload) {
        context.commit( 'MATERIAIS_ADICIONAR', payload )
    },
    materiais_remover(context, payload) {
        context.commit( 'MATERIAS_REMOVER', payload )
    },
    materiais_salvar(context, payload) {
        context.commit( 'MATERIAS_SALVAR', payload )
    },



//// Area Extra 

AreaExtra_calcular_valor_total(context, payload){
    context.commit( 'AREAS_EXTRAS_CALCULAR_VALOR_TOTAL', payload );
},


//// Area Interna Temp

AreaInterna_recalcular_detalhes_valor_pintura(context){
    context.commit( 'AREA_INTERNA_RECALCULAR_DETALHES_VALOR_PINTURA' );
},

AreaExtra_setar_nome(context, payload){
    context.commit( 'AREA_EXTRA_SETAR_NOME', payload )
},
AreaExtra_zerar_nome(context) {
    context.commit( 'AREA_EXTRA_ZERAR_NOME' )
},

AreaExtra_setar_metragem(context, payload) {
    context.commit( 'AREA_EXTRA_METRAGEM', payload )
},

AreaExtra_zerar_valor_pintura(context) {
    context.commit( 'AREA_EXTRA_ZERAR_DETALHES_VALOR_PINTURA')
},

AreaExtra_is_extra(context) {
    context.commit( 'AREA_EXTRA_IS_EXTRA' )
},
AreaExtra_isnt_extra(context) {
    context.commit( 'AREA_EXTRA_ISNT_EXTRA' )
},
AreaExtra_adicionar_servico(context, payload) {
    context.commit( 'AREA_EXTRA_ADICIONAR_SERVICOS', payload )
},
AreaExtra_adicionar_valor_servico(context, payload) {
    context.commit( 'AREA_EXTRA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload )
},
AreaExtra_remover_servico(context, payload) {
    context.commit( 'AREA_EXTRA_REMOVER_SERVICOS', payload )
},

AreaExtra_salvar_servico(context, payload) {
    context.commit( 'AREA_EXTRA_SALVAR_SERVICOS', payload )
},

AreaExtra_resetar(context) {
    context.commit( 'AREA_EXTRA_RESETAR' )
},

AreaExtra_setar_superficie(context, payload) {
    context.commit( 'AREA_EXTRA_SETAR_SUPERFICIE', payload )
},
AreaExtra_zerar_superficie(context) {
    context.commit( 'AREA_EXTRA_ZERAR_SUPERFICIE' )
},

SetarValorTotal(context) {
    context.commit( 'ENVIAR_ORCAMENTO' );
},

}

})
