module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		"bundle": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = global["webpackJsonp"] = global["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./main.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Calculadora.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CalculadoraComponents_Area__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Area.vue");
/* harmony import */ var _CalculadoraComponents_Extras__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Extras.vue");
/* harmony import */ var _Resumo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Resumo.vue");
/* harmony import */ var _CalculadoraComponents_MaoDeObra__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./components/CalculadoraComponents/MaoDeObra.vue");
/* harmony import */ var _CalculadoraComponents_Materiais__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./components/CalculadoraComponents/Materiais.vue");
/* harmony import */ var _Pedidos__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./components/Pedidos.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("../node_modules/tns-core-modules/http/http.js");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_7__);
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  data() {
    return {};
  },

  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_6__["mapGetters"])(['orcamento_area_extras_valor_total', 'orcamento_valor_total']), {
    valor_total: function valor_total() {
      return "Valor Total: R$".concat(this.orcamento_valor_total);
    }
  }),
  components: {
    'Area': _CalculadoraComponents_Area__WEBPACK_IMPORTED_MODULE_0__["default"],
    'Extras': _CalculadoraComponents_Extras__WEBPACK_IMPORTED_MODULE_1__["default"],
    'MaoDeObra': _CalculadoraComponents_MaoDeObra__WEBPACK_IMPORTED_MODULE_3__["default"],
    'Materiais': _CalculadoraComponents_Materiais__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_6__["mapActions"])(['AreaExterna_setar_valor_total', 'AreaInterna_setar_valor_total', 'AreaExtra_calcular_valor_total', 'SetarValorTotal', 'receber_pedidos', 'receber_pedidos', 'receber_superficies', 'receber_servicos', 'receber_areas_extras', 'receber_materiais', 'receber_valor_metro', 'zerar_pedidos']), {
    toResume: function toResume() {
      this.AreaExterna_setar_valor_total();
      this.AreaInterna_setar_valor_total();
      this.AreaExtra_calcular_valor_total(this.orcamento_area_extras_valor_total);
      this.SetarValorTotal();
      http__WEBPACK_IMPORTED_MODULE_7__["request"]({
        url: "https://sppinturas.herokuapp.com/orcamento",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        content: JSON.stringify(this.$store.state.pedido)
      }).then(response => {
        var result = response.content.toJSON();
      }, error => {
        console.error(error);
      }).then(console.log('Sended'));
      this.receber_pedidos();
      this.receber_valor_metro();
      this.receber_superficies();
      this.receber_servicos();
      this.receber_areas_extras();
      this.receber_materiais();
      this.zerar_pedidos();
      this.$navigateTo(_Pedidos__WEBPACK_IMPORTED_MODULE_5__["default"], {
        clearHistory: true
      });
    },
    backToPedidos: function backToPedidos() {
      this.receber_pedidos();
      this.receber_valor_metro();
      this.receber_superficies();
      this.receber_servicos();
      this.receber_areas_extras();
      this.receber_materiais();
      this.zerar_pedidos();
      this.$navigateBack();
    }
  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CalculadoraComponents_Area__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Area.vue");
/* harmony import */ var _shared_ActionBar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/shared/ActionBar.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['area', 'isEditExtra', 'index'],
  components: {
    'Area': _CalculadoraComponents_Area__WEBPACK_IMPORTED_MODULE_0__["default"],
    'ActionBar': _shared_ActionBar__WEBPACK_IMPORTED_MODULE_1__["default"]
  },

  data() {
    return {
      valor: '',
      nome: '',
      metragem: '',
      superficie: ''
    };
  },

  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])(['orcamento_area_extra_detalhes_valor_pintura', 'orcamento_area_extra_detalhes_valor_servicos', 'orcamento_area_extra_servicos']), {
    rightSideAction: function rightSideAction() {
      if (this.isEditExtra) {
        return 'Salvar';
      } else {
        return '+';
      }
    },
    leftSideAction: function leftSideAction() {
      if (this.isEditExtra) {
        return 'Excluir';
      } else {
        return 'Voltar';
      }
    }
  }),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])(['AreaExtra_resetar', 'AreaExtra_adicionar_area', 'AreaExtra_zerar_superficie', 'AreaExtra_salvar_area', 'AreaExtra_excluir_area']), {
    closeModal: function closeModal() {
      this.$modal.close();
    },
    saveAreaExtra: function saveAreaExtra() {
      this.AreaExtra_salvar_area({
        index: this.index,
        element: {
          nome: this.$store.state.areaExtraTemp.nome,
          valor_total: parseFloat((this.orcamento_area_extra_detalhes_valor_pintura + this.orcamento_area_extra_detalhes_valor_servicos).toFixed(2)),
          detalhes_valor: {
            valor_pintura: this.orcamento_area_extra_detalhes_valor_pintura,
            valor_servicos: this.orcamento_area_extra_detalhes_valor_servicos
          },
          metragem: this.$store.state.areaExtraTemp.metragem,
          superficie: this.$store.state.areaExtraTemp.superficie,
          servicos: this.orcamento_area_extra_servicos
        }
      });
      this.AreaExtra_resetar();
      this.AreaExtra_zerar_superficie();
      this.$modal.close();
    },
    addAreaExtra: function addAreaExtra() {
      this.AreaExtra_adicionar_area({
        nome: this.$store.state.areaExtraTemp.nome,
        valor_total: parseFloat((this.orcamento_area_extra_detalhes_valor_pintura + this.orcamento_area_extra_detalhes_valor_servicos).toFixed(2)),
        detalhes_valor: {
          valor_pintura: this.orcamento_area_extra_detalhes_valor_pintura,
          valor_servicos: this.orcamento_area_extra_detalhes_valor_servicos
        },
        metragem: this.$store.state.areaExtraTemp.metragem,
        superficie: this.$store.state.areaExtraTemp.superficie,
        servicos: this.orcamento_area_extra_servicos
      });
      this.AreaExtra_resetar();
      this.AreaExtra_zerar_superficie();
      this.$modal.close();
    },
    removeAreaExtra: function removeAreaExtra() {
      this.AreaExtra_excluir_area(this.index);
      this.AreaExtra_resetar();
      this.AreaExtra_zerar_superficie();
      this.$modal.close();
    }
  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Selecionar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Selecionar.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  mounted() {
    if (this.isEdit == false) {
      this.addControls = 'visible';
      this.editControls = 'collapsed';
    } else if (this.isEdit == true) {
      this.selectedMaterialButton = 'visible';
      this.selectMaterialButton = 'collapsed';
      this.selectedItem = this.data.material;
      this.quantidade = this.data.quantidade;
      this.addControls = 'collapsed';
      this.editControls = 'visible';
    }

    ;
  },

  props: ['type', 'isEdit', 'data', 'index'],

  data() {
    return {
      selectMaterialButton: 'visible',
      selectedMaterialButton: 'collapsed',
      selectedItem: '',
      quantidade: 1,
      editControls: '',
      addControls: ''
    };
  },

  computed: {},
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])(['materiais_adicionar', 'materiais_remover', 'materiais_salvar']), {
    selectMaterial: function selectMaterial() {
      this.$showModal(_Selecionar__WEBPACK_IMPORTED_MODULE_0__["default"], {
        fullscreen: true,
        props: {
          area: this.area,
          select: "material"
        }
      }).then(data => this.selectedItem = data);

      if (this.selectedItem == 'clean') {
        this.selectMaterialButton = 'visible';
        this.selectedMaterialButton = 'collapsed';
      } else {
        this.selectMaterialButton = 'collapsed';
        this.selectedMaterialButton = 'visible';
      }
    },
    onButtonTap: function onButtonTap(args) {
      this.materiais_adicionar({
        material: this.selectedItem,
        quantidade: this.quantidade
      });
      this.$modal.close();
    },
    removerMaterial: function removerMaterial() {
      this.materiais_remover(this.index);
      this.$modal.close();
    },
    salvarMaterial: function salvarMaterial() {
      console.log('teste');
      this.materiais_salvar({
        index: this.index,
        element: {
          material: this.selectedItem,
          quantidade: this.quantidade
        }
      });
      this.$modal.close();
    }
  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CalculadoraComponents_Area__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Area.vue");
/* harmony import */ var _shared_ActionBar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/shared/ActionBar.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    'Area': _CalculadoraComponents_Area__WEBPACK_IMPORTED_MODULE_0__["default"],
    'ActionBar': _shared_ActionBar__WEBPACK_IMPORTED_MODULE_1__["default"]
  },

  data() {
    return {
      altura: '',
      largura: '',
      itemEnabled: false
    };
  },

  computed: {
    sameSides: function sameSides() {
      if (this.itemEnabled == true) {
        return '2x';
      } else {
        return '1x';
      }
    },
    area: function area() {
      if (this.itemEnabled == false) {
        return parseFloat(parseFloat(this.altura * this.largura).toFixed(2));
      } else {
        return parseFloat(parseFloat(2 * (this.altura * this.largura)).toFixed(2));
      }
    },
    resultado: function resultado() {
      if (!isNaN(this.area) && this.area != 0.00) {
        return "\xC1rea Total: ".concat(this.area, " m\xB2");
      } else {
        return "Área Total: 0 m²";
      }
    }
  },
  methods: {
    closeModal: function closeModal() {
      this.$modal.close();
    },
    addArea: function addArea() {
      if (!isNaN(this.area) && this.area != 0.00) {
        this.$modal.close(this.area);
      }
    }
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Area.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CalculadoraComponents_Superficies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Superficies.vue");
/* harmony import */ var _CalculadoraComponents_Servicos__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Servicos.vue");
/* harmony import */ var _CalculadoraComponents_Metragem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/CalculadoraComponents/Metragem.vue");
/* harmony import */ var _CalculadoraComponents_Selecionar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./components/CalculadoraComponents/Selecionar.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  mounted() {
    if (this.isEditExtra == true) {
      this.showAddAreaButton();
      this.hideAddAreaButton();
      this.selectAreaButton = 'collapsed';
      this.selectedAreaButton = 'visible';
      this.superficie = this.$store.state.areaExtraTemp.superficie;
      this.addedArea = this.$store.state.areaExtraTemp.metragem;
    } else if (this.area == "Extras") {
      this.AreaExtra_is_extra();
      this.isExtra = 'visible';

      if (this.orcamento_area_extra_modal_isEdit == true) {
        this.selectedItem = this.orcamento_area_extra_modal_nome;
      }
    } else {
      this.AreaExtra_isnt_extra();
      this.isExtra = 'collapsed';
    }

    ;
  },

  props: ['area', 'isEditExtra'],
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_4__["mapGetters"])(['orcamento_area_externa_valor', 'orcamento_area_interna_valor', 'orcamento_area_extra_valor', 'orcamento_area_externa_servicos', 'orcamento_area_interna_servicos', 'orcamento_area_externa_detalhes_valor_pintura', 'orcamento_area_interna_detalhes_valor_pintura', 'orcamento_area_externa_detalhes_valor_servicos', 'orcamento_area_interna_detalhes_valor_servicos', 'orcamento_area_extra_modal_nome', 'orcamento_area_extra_modal_isEdit', 'orcamento_area_extra_valor', 'orcamento_area_extra_servicos', 'orcamento_area_extra_detalhes_valor_pintura', 'orcamento_area_extra_detalhes_valor_servicos', 'orcamento_area_extra_is_extra', 'orcamento_area_interna_is_interna']), {
    servicos_area: function servicos_area() {
      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        return this.orcamento_area_externa_servicos;
      } else if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false) {
        return this.orcamento_area_interna_servicos;
      } else if (this.orcamento_area_extra_is_extra) {
        return this.orcamento_area_extra_servicos;
      }
    },
    valorTotal: function valorTotal() {
      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        return "R$ ".concat(parseFloat((this.orcamento_area_externa_detalhes_valor_pintura + this.orcamento_area_externa_detalhes_valor_servicos).toFixed(2)));
      } else if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false) {
        return "R$ ".concat(parseFloat((this.orcamento_area_interna_detalhes_valor_pintura + this.orcamento_area_interna_detalhes_valor_servicos).toFixed(2)));
      } else if (this.orcamento_area_extra_is_extra) {
        return "R$ ".concat(parseFloat((this.orcamento_area_extra_detalhes_valor_pintura + this.orcamento_area_extra_detalhes_valor_servicos).toFixed(2)));
      }
    }
  }),

  data() {
    return {
      isExtra: 'collapsed',
      addAreaButton: 'visible',
      addedAreaButton: 'collapsed',
      addedArea: '',
      selectedItem: '',
      selectAreaButton: 'visible',
      selectedAreaButton: 'collapsed',
      superficie: ' . . .',
      editMetragens: []
    };
  },

  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_4__["mapActions"])(['AreaExterna_setar_metragem', 'AreaExterna_setar_detalhes_valor_pintura', 'AreaExterna_setar_fator_multiplicador', 'AreaExterna_recalcular_detalhes_valor_pintura', 'AreaExterna_setar_superficie', 'AreaInterna_setar_metragem', 'AreaInterna_setar_detalhes_valor_pintura', 'AreaInterna_setar_fator_multiplicador', 'AreaInterna_recalcular_detalhes_valor_pintura', 'AreaInterna_setar_superficie', 'AreaExtra_setar_nome', 'AreaExtra_zerar_nome', 'AreaExtra_is_extra', 'AreaExtra_isnt_extra', 'AreaExtra_setar_metragem', 'AreaExtra_setar_fator_multiplicador', 'AreaExtra_recalcular_detalhes_valor_pintura', 'AreaExtra_salvar_area', 'AreaExtra_setar_superficie', 'AreaExtra_setar_valor_total']), {
    area_externa_adicionar_detalhes_valor_pintura: function area_externa_adicionar_detalhes_valor_pintura(payload) {
      this.AreaExterna_setar_detalhes_valor_pintura(payload);
    },
    area_interna_adicionar_detalhes_valor_pintura: function area_interna_adicionar_detalhes_valor_pintura(payload) {
      this.AreaInterna_setar_detalhes_valor_pintura(payload);
    },
    //Local Methods
    openSuperficies: function openSuperficies() {
      this.$showModal(_CalculadoraComponents_Superficies__WEBPACK_IMPORTED_MODULE_0__["default"], {
        fullscreen: true,
        props: {
          area: this.area
        }
      }).then(data => this.setSurface(data));
    },
    openServicos: function openServicos() {
      let valorPinturaArea = '';

      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        valorPinturaArea = this.orcamento_area_externa_detalhes_valor_pintura;
      } else if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false) {
        valorPinturaArea = this.orcamento_area_interna_detalhes_valor_pintura;
      } else if (this.orcamento_area_extra_is_extra) {
        valorPinturaArea = this.orcamento_area_extra_detalhes_valor_pintura;
      }

      ;
      this.$showModal(_CalculadoraComponents_Servicos__WEBPACK_IMPORTED_MODULE_1__["default"], {
        fullscreen: true,
        props: {
          area: this.area,
          isEdit: false,
          valorPintura: valorPinturaArea
        }
      }).then(data => this.setServico(data));
    },
    addArea: function addArea() {
      this.$showModal(_CalculadoraComponents_Metragem__WEBPACK_IMPORTED_MODULE_2__["default"], {
        fullscreen: true,
        props: {
          area: this.areaChanged,
          editMetragens: this.editMetragens,
          toMetragensUndo: this.editMetragens,
          edit: true,
          addedArea: this.addedArea
        }
      }).then(data => this.setAddedArea(data));
    },
    setAddedArea: function setAddedArea(data) {
      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        this.AreaExterna_setar_metragem(data.totalArea);
        this.AreaExterna_recalcular_detalhes_valor_pintura();
      } else if (this.area =  true && this.orcamento_area_extra_is_extra == false) {
        this.AreaInterna_setar_metragem(data.totalArea);
        this.AreaInterna_recalcular_detalhes_valor_pintura();
      } else if (this.orcamento_area_extra_is_extra) {
        this.AreaExtra_setar_metragem(data.totalArea);
        this.AreaExtra_recalcular_detalhes_valor_pintura();
      }

      ;
      console.log(data.totalArea);
      this.AreaExtra_setar_metragem(data.totalArea);
      this.editMetragens = data.metragens;
      this.addedArea = data.totalArea;
      this.hideAddAreaButton();
    },
    setSurface: function setSurface(data) {
      if (data != undefined) {
        if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
          this.AreaExterna_setar_fator_multiplicador(data.fator_multiplicador);
          this.AreaExterna_recalcular_detalhes_valor_pintura();
          this.superficie = data.nome;
          this.AreaExterna_setar_superficie(data.nome);
        } else if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false || this.area == true) {
          this.AreaInterna_setar_fator_multiplicador(data.fator_multiplicador);
          this.AreaInterna_recalcular_detalhes_valor_pintura();
          this.superficie = data.nome;
          this.AreaInterna_setar_superficie(data.nome);
        } else if (this.orcamento_area_extra_is_extra) {
          this.AreaExtra_setar_fator_multiplicador(data.fator_multiplicador);
          this.AreaExtra_recalcular_detalhes_valor_pintura();
          this.superficie = data.nome;
          this.AreaExtra_setar_superficie(data.nome);
        }

        ;
      }
    },
    onItemTap: function onItemTap(args) {
      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        this.$showModal(_CalculadoraComponents_Servicos__WEBPACK_IMPORTED_MODULE_1__["default"], {
          fullscreen: true,
          props: {
            area: this.area,
            isEdit: true,
            index: args.index,
            descricao: this.orcamento_area_externa_servicos[args.index].descricao,
            porcentagem: this.orcamento_area_externa_servicos[args.index].porcentagem,
            valorPintura: this.orcamento_area_externa_detalhes_valor_pintura
          }
        });
      }

      if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false) {
        this.$showModal(_CalculadoraComponents_Servicos__WEBPACK_IMPORTED_MODULE_1__["default"], {
          fullscreen: true,
          props: {
            area: this.area,
            isEdit: true,
            index: args.index,
            descricao: this.orcamento_area_interna_servicos[args.index].descricao,
            porcentagem: this.orcamento_area_interna_servicos[args.index].porcentagem,
            valorPintura: this.orcamento_area_interna_detalhes_valor_pintura
          }
        });
      }

      if (this.orcamento_area_extra_is_extra) {
        this.$showModal(_CalculadoraComponents_Servicos__WEBPACK_IMPORTED_MODULE_1__["default"], {
          fullscreen: true,
          props: {
            area: this.area,
            isEdit: true,
            index: args.index,
            descricao: this.orcamento_area_extra_servicos[args.index].descricao,
            porcentagem: this.orcamento_area_extra_servicos[args.index].porcentagem,
            valorPintura: this.orcamento_area_extra_detalhes_valor_pintura
          }
        });
      }
    },
    setServico: function setServico(data) {
      if (data != undefined) {}
    },
    hideAddAreaButton: function hideAddAreaButton() {
      this.addAreaButton = 'collapsed';
      this.addedAreaButton = 'visible';
    },
    showAddAreaButton: function showAddAreaButton() {
      this.addAreaButton = 'visible';
      this.addedAreaButton = 'collapsed';
    },
    selectArea: function selectArea() {
      this.$showModal(_CalculadoraComponents_Selecionar__WEBPACK_IMPORTED_MODULE_3__["default"], {
        fullscreen: true,
        props: {
          area: this.area,
          select: "extras"
        }
      }).then(data => this.setAreaExtra(data));
      this.selectAreaButton = 'collapsed';
      this.selectedAreaButton = 'visible';
    },
    setAreaExtra: function setAreaExtra(data) {
      this.selectedItem = data;
      this.AreaExtra_setar_nome(data);
    }
  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Extras.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CalculadoraComponents_Superficies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Superficies.vue");
/* harmony import */ var _CalculadoraComponents_Servicos__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Servicos.vue");
/* harmony import */ var _CalculadoraComponents_AdicionarExtra__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/CalculadoraComponents/AdicionarExtra.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['area'],
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_3__["mapGetters"])(['orcamento_area_extras_areas', 'orcamento_area_extras_valor_total']), {
    valorTotalExtras: function valorTotalExtras() {
      return "R$ ".concat(this.orcamento_area_extras_valor_total);
    },
    extras: function extras() {
      return this.orcamento_area_extras_areas;
    }
  }),

  data() {
    return {};
  },

  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_3__["mapActions"])(['AreaExtra_adicionar_area', 'AreaExtra_setar_area']), {
    openSuperficies: function openSuperficies() {
      this.$showModal(_CalculadoraComponents_Superficies__WEBPACK_IMPORTED_MODULE_0__["default"], {
        fullscreen: true,
        props: {
          area: this.area
        }
      });
    },
    openServicos: function openServicos() {
      this.$showModal(_CalculadoraComponents_Servicos__WEBPACK_IMPORTED_MODULE_1__["default"], {
        fullscreen: true,
        props: {
          area: this.area
        }
      });
    },
    modalAddExtras: function modalAddExtras() {
      this.$showModal(_CalculadoraComponents_AdicionarExtra__WEBPACK_IMPORTED_MODULE_2__["default"], {
        fullscreen: true,
        props: {
          area: 'Extras'
        }
      });
    },
    editAreasExtras: function editAreasExtras(args) {
      this.AreaExtra_setar_area({
        isEditExtra: true,
        isExtra: true,
        nome: this.extras[args.index].nome,
        valor_total: this.extras[args.index].valor_total,
        detalhes_valor: {
          valor_pintura: this.extras[args.index].detalhes_valor.valor_pintura,
          valor_servicos: this.extras[args.index].detalhes_valor.valor_servicos
        },
        metragem: this.extras[args.index].metragem,
        superficie: this.extras[args.index].superficie,
        servicos: this.extras[args.index].servicos
      });
      this.$showModal(_CalculadoraComponents_AdicionarExtra__WEBPACK_IMPORTED_MODULE_2__["default"], {
        fullscreen: true,
        props: {
          area: 'Extras',
          index: args.index,
          isEditExtra: true
        }
      });
    }
  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/MaoDeObra.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['type'],

  data() {
    return {
      pintores: 0
    };
  },

  methods: {}
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Materiais.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AdicionarMaterial__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/AdicionarMaterial.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['type'],

  data() {
    return {};
  },

  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])(['set_materiais']), {
    materiais: function materiais() {
      return this.set_materiais;
    }
  }),
  methods: {
    showAddMaterial: function showAddMaterial() {
      this.$showModal(_AdicionarMaterial__WEBPACK_IMPORTED_MODULE_0__["default"], {
        fullscreen: true,
        props: {
          type: this.type,
          select: "material",
          isEdit: false
        }
      });
    },
    onItemTapMaterial: function onItemTapMaterial(args) {
      console.log(args.index);
      this.$showModal(_AdicionarMaterial__WEBPACK_IMPORTED_MODULE_0__["default"], {
        fullscreen: true,
        props: {
          type: this.type,
          select: "material",
          isEdit: true,
          index: args.index,
          data: {
            material: this.$store.state.pedido.orcamento.materiais[args.index].material,
            quantidade: this.$store.state.pedido.orcamento.materiais[args.index].quantidade
          }
        }
      });
    }
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Metragem.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shared_ActionBar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/shared/ActionBar.vue");
/* harmony import */ var _AdicionarMetragem__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/AdicionarMetragem.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['area', 'editMetragens', 'addedArea'],

  mounted() {
    this.metragens = this.editMetragens;
    this.oldArea = this.addedArea;
  },

  components: {
    'ActionBar': _shared_ActionBar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },

  data() {
    return {
      metragens: []
    };
  },

  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])(['orcamento_area_extra_is_extra']), {
    totalAreaResult: function totalAreaResult() {
      return "".concat(this.totalArea, " m\xB2");
    },
    totalArea: function totalArea() {
      return parseFloat(this.metragens.reduce((a, b) => a + b, 0).toFixed(2));
    }
  }),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])(['AreaInterna_zerar_valor_pintura', 'AreaExterna_zerar_valor_pintura', 'AreaExtra_zerar_valor_pintura']), {
    modalAddArea: function modalAddArea() {
      this.$showModal(_AdicionarMetragem__WEBPACK_IMPORTED_MODULE_1__["default"], {
        fullscreen: true,
        props: {
          area: "this.area"
        }
      }).then(data => this.addMetragens(data));
    },
    addMetragens: function addMetragens(data) {
      if (data !== undefined) {
        this.metragens.push(data);
      }
    },
    onItemTap: function onItemTap(args) {
      this.metragens.splice(args.index, 1);
    },
    addMeters: function addMeters() {
      if (this.totalArea == 0) {
        if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
          this.AreaExterna_zerar_valor_pintura();
          this.$modal.close({
            area: this.area,
            totalArea: this.totalArea,
            metragens: this.metragens
          });
        } else if (this.area == 'Interna' && this.orcamento_area_extra_is_extra == false) {
          this.AreaInterna_zerar_valor_pintura();
          this.$modal.close({
            area: this.area,
            totalArea: this.totalArea,
            metragens: this.metragens
          });
        } else if (this.orcamento_area_extra_is_extra) {
          this.AreaExtra_zerar_valor_pintura();
          this.$modal.close({
            area: this.area,
            totalArea: this.totalArea,
            metragens: this.metragens
          });
        }

        ;
      } else {
        this.$modal.close({
          area: this.area,
          totalArea: this.totalArea,
          metragens: this.metragens
        });
      }
    }
  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Selecionar.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shared_ActionBar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/shared/ActionBar.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    "ActionBar": _shared_ActionBar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['area', 'select'],
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])(['setar_servicos', 'setar_areas_extras', 'setar_materiais']), {
    lista_servicos: function lista_servicos() {
      return this.setar_servicos;
    },
    lista_area_extra: function lista_area_extra() {
      return this.setar_areas_extras;
    },
    lista_materiais: function lista_materiais() {
      return this.setar_materiais;
    },
    listRender: function listRender() {
      if (this.select == "extras") {
        return this.lista_area_extra.filter(servico => {
          return servico.toLowerCase().match(this.searchQuery.toLowerCase());
        });
      } else if (this.select === "material") {
        return this.lista_materiais.filter(servico => {
          return servico.toLowerCase().match(this.searchQuery.toLowerCase());
        });
      } else {
        return this.lista_servicos.filter(servico => {
          return servico.toLowerCase().match(this.searchQuery.toLowerCase());
        });
      }
    }
  }),

  data() {
    return {
      searchQuery: ""
    };
  },

  methods: {
    onItemTap: function onItemTap(args) {
      if (this.select == 'extras') {
        var item = this.listRender[args.index];
        console.log(item);
        this.$modal.close(item);
      }

      if (this.select == 'material') {
        var item = this.listRender[args.index];
        console.log(item);
        this.$modal.close(item);
      } else {
        var item = this.listRender[args.index];
        console.log(item);
        this.$modal.close(item);
      }
    }
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Servicos.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Selecionar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Selecionar.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  mounted() {
    if (this.isEdit == false) {
      this.addControls = 'visible';
      this.editControls = 'collapsed';
    } else {
      this.addControls = 'collapsed';
      this.editControls = 'visible';
      this.hideSelectServiceButton();
      this.selectedItem = this.descricao;
      this.porcentagemSlider = this.porcentagem;
    }

    ;
    this.valorAntigo = this.valor;
  },

  props: ['area', 'isEdit', 'descricao', 'porcentagem', 'valorPintura', 'index'],

  data() {
    return {
      selectServiceButton: 'visible',
      selectedServiceButton: 'collapsed',
      selectedItem: '',
      porcentagemSlider: 1,
      editControls: '',
      addControls: '',
      valorAntigo: ''
    };
  },

  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])(['orcamento_area_extra_is_extra']), {
    valor: function valor() {
      return parseFloat((this.valorPintura * this.porcentagemSlider / 100).toFixed(2));
    }
  }),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])(['AreaExterna_adicionar_servico', 'AreaExterna_remover_servico', 'AreaExterna_salvar_servico', 'AreaExterna_adicionar_valor_servicos', 'AreaExterna_remover_valor_servicos', 'AreaInterna_adicionar_servico', 'AreaInterna_remover_servico', 'AreaInterna_salvar_servico', 'AreaInterna_adicionar_valor_servicos', 'AreaInterna_remover_valor_servicos', 'AreaExtra_adicionar_valor_servico', 'AreaExtra_adicionar_servico', 'AreaExtra_remover_servico', 'AreaExtra_salvar_servico', 'AreaExterna_recalcular_valor_servico', 'AreaInterna_recalcular_valor_servicos']), {
    modalClose: function modalClose() {
      this.$modal.close();
    },
    selectService: function selectService() {
      this.$showModal(_Selecionar__WEBPACK_IMPORTED_MODULE_0__["default"], {
        fullscreen: true,
        props: {
          select: 'servicos'
        }
      }).then(data => this.setSelectedItem(data));
    },
    setSelectedItem: function setSelectedItem(data) {
      this.selectedItem = data;
      this.hideSelectServiceButton();
    },
    adicionarServico: function adicionarServico() {
      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        if (this.selectServiceButton == 'visible') {
          this.hideSelectServiceButton();
        } else {
          this.showSelectServiceButton();
        }

        ;
        this.AreaExterna_adicionar_servico({
          descricao: this.selectedItem,
          porcentagem: this.porcentagemSlider,
          valor: this.valor
        });
        this.AreaExterna_recalcular_valor_servico();
        this.modalClose();
      }

      if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false) {
        if (this.selectServiceButton == 'visible') {
          this.hideSelectServiceButton();
        } else {
          this.showSelectServiceButton();
        }

        ;
        this.AreaInterna_adicionar_servico({
          descricao: this.selectedItem,
          porcentagem: this.porcentagemSlider,
          valor: this.valor
        });
        this.AreaInterna_recalcular_valor_servicos();
        this.modalClose();
      }

      if (this.orcamento_area_extra_is_extra) {
        if (this.selectServiceButton == 'visible') {
          this.hideSelectServiceButton();
        } else {
          this.showSelectServiceButton();
        }

        ;
        this.AreaExtra_adicionar_servico({
          descricao: this.selectedItem,
          porcentagem: this.porcentagemSlider,
          valor: this.valor
        });
        this.AreaExtra_adicionar_valor_servico(this.valor);
        this.modalClose();
      }
    },
    removerServico: function removerServico() {
      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        this.AreaExterna_remover_servico(this.index);
        this.AreaExterna_recalcular_valor_servico();
        this.modalClose();
      } else if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false) {
        this.AreaInterna_remover_servico(this.index);
        this.AreaInterna_recalcular_valor_servicos();
        this.modalClose();
      } else if (this.orcamento_area_extra_is_extra) {
        this.AreaExtra_remover_servico(this.index);
        this.modalClose();
      }
    },
    salvarServico: function salvarServico() {
      if (this.area == 'Externa' && this.orcamento_area_extra_is_extra == false) {
        this.AreaExterna_salvar_servico({
          index: this.index,
          element: {
            descricao: this.selectedItem,
            porcentagem: this.porcentagemSlider,
            valor: this.valor
          }
        });
        this.AreaExterna_recalcular_valor_servico();
        this.modalClose();
      } else if ((this.area == 'Interna' || this.area == true) && this.orcamento_area_extra_is_extra == false) {
        this.AreaInterna_salvar_servico({
          index: this.index,
          element: {
            descricao: this.selectedItem,
            porcentagem: this.porcentagemSlider,
            valor: this.valor
          }
        });
        this.AreaInterna_recalcular_valor_servicos();
        this.modalClose();
      } else if (this.orcamento_area_extra_is_extra) {
        this.AreaExtra_salvar_servico({
          index: this.index,
          element: {
            descricao: this.selectedItem,
            porcentagem: this.porcentagemSlider,
            valor: this.valor
          }
        });
        this.modalClose();
      }
    },
    showSelectServiceButton: function showSelectServiceButton() {
      this.selectServiceButton = 'visible';
      this.selectedServiceButton = 'collapsed';
    },
    hideSelectServiceButton: function hideSelectServiceButton() {
      this.selectServiceButton = 'collapsed';
      this.selectedServiceButton = 'visible';
    }
  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Superficies.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['area'],
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['setar_superficies']), {
    superficies: function superficies() {
      return this.setar_superficies;
    }
  }),

  data() {
    return {
      selectedSuperficieIndex: 0
    };
  },

  methods: {
    modalClose: function modalClose() {
      this.$modal.close();
    },
    selecionarSuperficie: function selecionarSuperficie() {
      this.$modal.close({
        nome: this.superficies[this.selectedSuperficieIndex].nome,
        fator_multiplicador: this.superficies[this.selectedSuperficieIndex].fator_multiplicador
      });
    }
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Login.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ResetSenha__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/ResetSenha.vue");
/* harmony import */ var _Pedidos__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Pedidos.vue");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/tns-core-modules/platform/platform.js");
/* harmony import */ var tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var tns_core_modules_color__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/tns-core-modules/color/color.js");
/* harmony import */ var tns_core_modules_color__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_color__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  data() {
    return {
      user_mail: '',
      user_password: ''
    };
  },

  methods: {
    forgotPassword() {
      this.$showModal(_ResetSenha__WEBPACK_IMPORTED_MODULE_0__["default"], {});
    },

    login() {
      this.$navigateTo(_Pedidos__WEBPACK_IMPORTED_MODULE_1__["default"], {
        clearHistory: true
      });
    },

    pageLoaded() {
      if (tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2__["android"] && tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3__["device"].sdkVersion >= "21") {
        const window = tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2__["android"].foregroundActivity.getWindow();
        window.setStatusBarColor(new tns_core_modules_color__WEBPACK_IMPORTED_MODULE_4__["Color"]("#262626").android);
      }
    },

    alert() {
      return alert({
        title: 'Dados inválidos',
        okButtonText: 'OK',
        message: 'Email ou Senha não foram encontrados.'
      });
    },

    login2() {
      let data = {
        email: this.user_mail,
        password: this.user_password
      }; // falta a comparação dos dados

      http.request({
        url: "https://sppinturas.herokuapp.com/auth",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        content: JSON.stringify(data)
      }).then(response => {
        var result = response.content.toJSON();
      }, error => {
        this.alert();
        console.error(error);
      }).then(console.log('Login data sended'));
    }

  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Pedidos.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shared_ActionBar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/shared/ActionBar.vue");
/* harmony import */ var _Calculadora__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Calculadora.vue");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/http/http.js");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  mounted() {
    this.receber_pedidos();
    this.receber_valor_metro();
    this.receber_superficies();
    this.receber_servicos();
    this.receber_areas_extras();
    this.receber_materiais();
    this.zerar_pedidos();
  },

  data() {
    return {
      pedidos: []
    };
  },

  components: {
    'ActionBar': _shared_ActionBar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_3__["mapActions"])(['receber_pedidos', 'receber_superficies', 'receber_servicos', 'receber_areas_extras', 'receber_materiais', 'receber_configuracoes_pedido', 'receber_valor_metro', 'zerar_pedidos']), {
    toCalculador(args) {
      console.log(args.index);
      this.receber_configuracoes_pedido({
        id_pedido: this.$store.state.pedidos[args.index].id,
        numero_de_predios: this.$store.state.pedidos[args.index].condominio.numero_de_predios,
        numero_de_andares: this.$store.state.pedidos[args.index].condominio.numero_de_andares_por_predio
      });
      this.$navigateTo(_Calculadora__WEBPACK_IMPORTED_MODULE_1__["default"], {});
    },

    logout() {
      this.$navigateTo(Login, {
        clearHistory: true
      });
    }

  })
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/ResetSenha.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    closeModal: function closeModal() {
      this.$modal.close();
    }
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Resumo.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/tns-core-modules/http/http.js");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  mounted() {
    // this.AreaExterna_setar_valor_total();
    // this.AreaInterna_setar_valor_total();
    // this.AreaExtra_calcular_valor_total();
    http__WEBPACK_IMPORTED_MODULE_0__["request"]({
      url: "https://sppinturas.herokuapp.com/orcamento",
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      content: JSON.stringify(this.$store.state.pedido)
    }).then(response => {
      var result = response.content.toJSON();
    }, error => {
      console.error(error);
    }).then(console.log('Sended'));
  },

  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])(['AreaExterna_setar_valor_total', 'AreaInterna_setar_valor_total', 'AreaExtra_calcular_valor_total'])),

  data() {
    return {
      teste: {
        id_pedido: 1,
        valor_total: 426100,
        orcamento: {
          orcamento_area_externa: {
            valor_total: 175200,
            detalhes_valor: {
              valor_pintura: 160000,
              valor_servicos: 15200
            },
            metragem: 400,
            superficie: "Travertino",
            servicos: [{
              descricao: "Lavagem da caixa d'água",
              porcentagem: 5,
              valor: 4000
            }, {
              descricao: "Vedação dos caixilhos com infiltração",
              porcentagem: 7,
              valor: 11200
            }]
          },
          orcamento_area_interna: {
            valor_total: 16500,
            detalhes_valor: {
              valor_pintura: 10000,
              valor_servicos: 6500
            },
            metragem: 100,
            superficie: "Lisa",
            servicos: [{
              descricao: "Tratamento de trincas",
              porcentagem: 15,
              valor: 1500
            }, {
              descricao: "Vedação dos caixilhos com infiltração",
              porcentagem: 10,
              valor: 1000
            }, {
              descricao: "Pintura da tubulação das fachadas",
              porcentagem: 5,
              valor: 500
            }, {
              descricao: "Limpeza após final da obra",
              porcentagem: 35,
              valor: 3500
            }]
          },
          orcamento_area_extra: {
            valor_total: 234400,
            areas: [{
              nome: "Garagem",
              valor_total: 122850,
              detalhes_valor: {
                valor_pintura: 105000,
                valor_servicos: 17850
              },
              metragem: 300,
              superficie: "Textura",
              servicos: [{
                descricao: "Pintura da tubulação das fachadas",
                porcentagem: 5,
                valor: 5250
              }, {
                descricao: "Tratamento de trincas",
                porcentagem: 12,
                valor: 12600
              }]
            }, {
              nome: "Playground",
              valor_total: 105300,
              detalhes_valor: {
                valor_pintura: 81000,
                valor_servicos: 24300
              },
              metragem: 300,
              superficie: "Massa Raspada",
              servicos: [{
                descricao: "Limpeza dos brinquedos",
                porcentagem: 20,
                valor: 16200
              }, {
                descricao: "Troca de bebedouro",
                porcentagem: 10,
                valor: 8100
              }]
            }, {
              nome: "Salão de festas",
              valor_total: 6250,
              detalhes_valor: {
                valor_pintura: 5000,
                valor_servicos: 1250
              },
              metragem: 50,
              superficie: "Lisa",
              servicos: [{
                descricao: "Tratamento de trincas",
                porcentagem: 15,
                valor: 750
              }, {
                descricao: "Limpeza das paredes",
                porcentagem: 10,
                valor: 500
              }]
            }]
          },
          materiais: [{
            material: "Teste3",
            quantidade: 20
          }, {
            material: "Teste3",
            quantidade: 10
          }] // id_pedido: 1,
          // valor_total: 0,
          // orcamento: {
          //     orcamento_area_externa: {
          //         valor_total: 0,
          //         detalhes_valor: {
          //             valor_pintura: 0,
          //             valor_servicos: 0
          //         },
          //         metragem: 0,
          //         superficie: "",
          //         servicos: []
          //     },
          //     orcamento_area_interna: {
          //         valor_total: 0,
          //         detalhes_valor: {
          //             valor_pintura: 0,
          //             valor_servicos: 0
          //         },
          //         metragem: 0,
          //         superficie: "",
          //         servicos: []
          //     },
          //     orcamento_area_extra: {
          //         valor_total: 0,
          //         areas: []
          //     },
          //     materiais: []
          // }

        }
      }
    };
  }

});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/shared/ActionBar.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Login.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['left', 'right', 'title', 'area'],

  data() {
    return {};
  },

  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])([])),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])(['AreaExtra_resetar', 'AreaExtra_zerar_superficie']), {
    leftFunction: function leftFunction() {
      if (this.left == 'Voltar') {
        this.AreaExtra_resetar();
        this.AreaExtra_zerar_superficie();
        this.$modal.close();
      } else if (this.left == 'Logout') {
        this.$navigateTo(_Login__WEBPACK_IMPORTED_MODULE_0__["default"], {
          clearHistory: true
        });
      } else if (this.left == 'Excluir') {
        console.log('Excluir');
        this.$emit('removeAreaExtra');
      }
    },
    rightFunction: function rightFunction() {
      if (this.right == '+') {
        this.$emit('addAreaExtra');
      } else if (this.right == 'Salvar') {
        this.$emit('saveAreaExtra');
      } else {
        this.$emit('addMeters');
      }
    }
  })
});

/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Calculadora.vue?vue&type=template&id=3a12e224&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    [
      _c(
        "android",
        [
          _c(
            "ActionBar",
            { attrs: { title: "Orçamento" } },
            [
              _c("ActionItem", {
                attrs: { icon: "~/images/resumo.png" },
                on: { tap: _vm.toResume }
              }),
              _c("NavigationButton", {
                attrs: { "android.systemIcon": "ic_menu_back" },
                on: { tap: _vm.backToPedidos }
              })
            ],
            1
          )
        ],
        1
      ),
      _c(
        "ios",
        [
          _c(
            "ActionBar",
            { staticClass: "actionBar", attrs: { title: "Orçamento" } },
            [
              _c("ActionItem", {
                attrs: {
                  icon: "~/images/resumoios.png",
                  "ios.position": "right"
                },
                on: { tap: _vm.toResume }
              }),
              _c("NavigationButton", {
                attrs: {
                  text: "Pedidos",
                  "android.systemIcon": "ic_menu_back"
                },
                on: { tap: _vm.backToPedidos }
              })
            ],
            1
          )
        ],
        1
      ),
      _c(
        "FlexboxLayout",
        { staticClass: "page" },
        [
          _c(
            "StackLayout",
            [
              _c("Label", {
                staticClass: "center",
                attrs: { fontSize: "16", padding: "10", text: this.valor_total }
              }),
              _c("StackLayout", { staticClass: "hr-light" }),
              _c(
                "TabView",
                {
                  attrs: {
                    background: "#262626",
                    tabTextColor: "#6A84AC",
                    androidTabsPosition: "bottom",
                    selectedTabTextColor: "#1A75FE",
                    tabBackgroundColor: "#373737"
                  }
                },
                [
                  _c(
                    "TabViewItem",
                    { staticClass: "icon", attrs: { title: "" } },
                    [
                      _c(
                        "StackLayout",
                        { attrs: { padding: "10 5 0 5" } },
                        [
                          _c("Label", {
                            staticClass: "labelCalculadora strong",
                            attrs: { fontSize: "18", text: "Área Externa" }
                          }),
                          _c("Area", { attrs: { area: "Externa" } })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _c(
                    "TabViewItem",
                    { staticClass: "icon", attrs: { title: "" } },
                    [
                      _c(
                        "StackLayout",
                        { attrs: { padding: "10 5 0 5" } },
                        [
                          _c("Label", {
                            staticClass: "labelCalculadora strong",
                            attrs: {
                              fontSize: "18",
                              textWrap: "true",
                              text: "Área Interna"
                            }
                          }),
                          _c("Area", { attrs: { area: "Interna" } })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _c(
                    "TabViewItem",
                    { staticClass: "icon", attrs: { title: "" } },
                    [
                      _c(
                        "StackLayout",
                        { attrs: { padding: "10 5 0 5" } },
                        [
                          _c("Label", {
                            staticClass: "labelCalculadora strong",
                            attrs: {
                              fontSize: "18",
                              textWrap: "true",
                              text: "Extras"
                            }
                          }),
                          _c("Extras", { attrs: { area: "Extras" } })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _c(
                    "TabViewItem",
                    { staticClass: "icon", attrs: { title: "" } },
                    [
                      _c(
                        "StackLayout",
                        { attrs: { padding: "10 5 0 5" } },
                        [
                          _c("Label", {
                            staticClass: "labelCalculadora strong",
                            attrs: {
                              fontSize: "18",
                              textWrap: "true",
                              text: "Materiais"
                            }
                          }),
                          _c("Materiais", { attrs: { type: "Materiais" } })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=template&id=22f3508e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    { staticClass: "pageModal", attrs: { backgroundColor: "#707070" } },
    [
      _c(
        "DockLayout",
        { attrs: { stretchLastChild: "true" } },
        [
          _c("ActionBar", {
            attrs: {
              dock: "top",
              title: "Adicionar Área Extra",
              left: this.leftSideAction,
              area: "Extras",
              right: this.rightSideAction
            },
            on: {
              removeAreaExtra: _vm.removeAreaExtra,
              saveAreaExtra: _vm.saveAreaExtra,
              addAreaExtra: _vm.addAreaExtra
            }
          }),
          _c("Area", {
            attrs: {
              dock: "bottom",
              isEditExtra: this.isEditExtra,
              backgroundColor: "#262626",
              area: "Extras",
              selectExtra: "true"
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=template&id=a88ca4e4&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    { staticClass: "page", attrs: { backgroundColor: "#262626" } },
    [
      _c(
        "StackLayout",
        {
          staticClass: "marginSides title",
          attrs: { verticalAlignment: "center" }
        },
        [
          _c(
            "FlexboxLayout",
            { attrs: { height: "40", flexDirection: "row" } },
            [
              _c("Label", {
                staticClass: "closeModal",
                attrs: { text: "X", width: "100%" },
                on: { tap: _vm.$modal.close }
              })
            ],
            1
          ),
          _c(
            "StackLayout",
            { staticClass: "center", attrs: { verticalAlignment: "center" } },
            [
              _c("Label", {
                staticClass: "strong title marginBotton",
                attrs: { textWrap: "true", text: "Adicionar Material" }
              }),
              _c("Label", {
                staticClass: "margTopLess marginBotton",
                attrs: {
                  backgroundColor: "transparent",
                  color: "#1A75FE",
                  text: "Selecionar Material",
                  visibility: _vm.selectMaterialButton
                },
                on: { tap: _vm.selectMaterial }
              }),
              _c("Label", {
                staticClass: "margTopLess marginBotton",
                attrs: {
                  backgroundColor: "transparent",
                  textWrap: "true",
                  fontSize: "14",
                  horizontalAlignment: "center",
                  text: this.selectedItem,
                  visibility: _vm.selectedMaterialButton
                },
                on: { tap: _vm.selectMaterial }
              }),
              _c("Label", {
                staticClass: "medium padTop marginBotton",
                attrs: { text: "Quantidade" }
              }),
              _c("Label", {
                staticClass: "center padTop marginBotton",
                attrs: { text: _vm.quantidade }
              }),
              _c("Slider", {
                staticClass: "marg",
                attrs: {
                  minValue: "1",
                  maxValue: "100",
                  value: _vm.quantidade
                },
                on: {
                  valueChange: function($event) {
                    _vm.quantidade = $event.value
                  }
                }
              }),
              _c("Button", {
                staticClass: "marginTop",
                attrs: { visibility: _vm.addControls, text: "Adicionar" },
                on: { tap: _vm.onButtonTap }
              }),
              _c(
                "GridLayout",
                {
                  attrs: {
                    visibility: _vm.editControls,
                    height: "80",
                    width: "100%",
                    rows: "*",
                    columns: "5*, *, 5*"
                  }
                },
                [
                  _c("Button", {
                    staticClass: "margTopLess",
                    attrs: {
                      row: "0",
                      col: "0",
                      backgroundColor: "#D12020",
                      text: "Excluir"
                    },
                    on: { tap: _vm.removerMaterial }
                  }),
                  _c("Button", {
                    staticClass: "margTopLess",
                    attrs: {
                      row: "0",
                      col: "2",
                      backgroundColor: "#0EA174",
                      text: "Salvar"
                    },
                    on: { tap: _vm.salvarMaterial }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=template&id=3de7244b&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    { staticClass: "page", attrs: { backgroundColor: "#262626" } },
    [
      _c(
        "StackLayout",
        { staticClass: "marginSides verticalCenter" },
        [
          _c(
            "FlexboxLayout",
            { attrs: { height: "40", flexDirection: "row" } },
            [
              _c("Label", {
                staticClass: "closeModal",
                attrs: { text: "X", width: "100%" },
                on: { tap: _vm.closeModal }
              })
            ],
            1
          ),
          _c("Label", {
            staticClass: "center title marginBotton",
            attrs: { text: "Adicione altura e largura", row: "1", col: "0" }
          }),
          _c(
            "StackLayout",
            {
              staticClass: "marginTop input",
              attrs: { width: "80%", row: "2", col: "0" }
            },
            [
              _c("TextField", {
                staticClass: "input marginBotton",
                attrs: {
                  borderWidth: "0",
                  hint: "Altura",
                  keyboardType: "number",
                  returnKeyType: "Próximo",
                  text: _vm.altura
                },
                on: {
                  textChange: function($event) {
                    _vm.altura = $event.value
                  }
                }
              }),
              _c("ios", [_c("StackLayout", { staticClass: "hr-light" })], 1),
              _c("TextField", {
                staticClass: "input marginTop marginBotton",
                attrs: {
                  hint: "Largura",
                  keyboardType: "number",
                  returnKeyType: "Próximo",
                  text: _vm.largura
                },
                on: {
                  textChange: function($event) {
                    _vm.largura = $event.value
                  }
                }
              }),
              _c("ios", [_c("StackLayout", { staticClass: "hr-light" })], 1)
            ],
            1
          ),
          _c(
            "StackLayout",
            { staticClass: "marginTop", attrs: { row: "2", col: "0" } },
            [
              _c(
                "GridLayout",
                { attrs: { columns: "5*, *", rows: "60" } },
                [
                  _c("Label", {
                    staticClass: "center",
                    attrs: { row: "0", col: "0", text: _vm.resultado }
                  }),
                  _c(
                    "StackLayout",
                    { attrs: { row: "0", col: "1" } },
                    [
                      _c("Label", { attrs: { text: _vm.sameSides } }),
                      _c("Switch", {
                        attrs: {
                          backgroundColor: "white",
                          id: "userStatus",
                          checked: "false",
                          checked: _vm.itemEnabled
                        },
                        on: {
                          checkedChange: function($event) {
                            _vm.itemEnabled = $event.value
                          }
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _c("Button", {
                staticClass: "marginTop",
                attrs: { text: "Adicionar" },
                on: { tap: _vm.addArea }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Area.vue?vue&type=template&id=7bbc431c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "StackLayout",
    [
      _c(
        "GridLayout",
        { attrs: { height: "30", columns: "8*, 14*, 3*", rows: "*" } },
        [
          _c("Label", {
            staticClass: "strong left",
            attrs: { text: "Valor", row: "0", col: "0" }
          }),
          _c("Label", { attrs: { text: _vm.valorTotal, row: "0", col: "1" } })
        ],
        1
      ),
      _c(
        "GridLayout",
        {
          attrs: {
            visibility: _vm.isExtra,
            height: "50",
            columns: "8*, 14*, 3*",
            rows: "*"
          }
        },
        [
          _c("Label", {
            staticClass: "strong labelInput left",
            attrs: { text: "Nome", row: "0", col: "0" }
          }),
          _c("Label", {
            staticClass: "margTopLess marginBotton",
            attrs: {
              backgroundColor: "transparent",
              color: "#1A75FE",
              row: "0",
              col: "1",
              text: "Selecionar Área Extra",
              visibility: _vm.selectAreaButton
            },
            on: { tap: _vm.selectArea }
          }),
          _c("Label", {
            staticClass: "margTopLess marginBotton",
            attrs: {
              backgroundColor: "transparent",
              fontSize: "14",
              horizontalAlignment: "center",
              row: "0",
              col: "1",
              text: _vm.selectedItem,
              visibility: _vm.selectedAreaButton
            },
            on: { tap: _vm.selectArea }
          })
        ],
        1
      ),
      _c(
        "GridLayout",
        { attrs: { height: "50", columns: "8*, 14*, 3*", rows: "*" } },
        [
          _c("Label", {
            staticClass: "strong labelInput left",
            attrs: { text: "Metragem", row: "0", col: "0" }
          }),
          _c("Label", {
            staticClass: "margTopLess marginBotton",
            attrs: {
              backgroundColor: "transparent",
              color: "#1A75FE",
              row: "0",
              col: "1",
              text: "Adicionar Metragem",
              visibility: _vm.addAreaButton
            },
            on: { tap: _vm.addArea }
          }),
          _c("Label", {
            staticClass: "margTopLess marginBotton",
            attrs: {
              backgroundColor: "transparent",
              fontSize: "14",
              horizontalAlignment: "center",
              row: "0",
              col: "1",
              text: _vm.addedArea + " m²",
              visibility: _vm.addedAreaButton
            },
            on: { tap: _vm.addArea }
          })
        ],
        1
      ),
      _c(
        "GridLayout",
        { attrs: { height: "50", columns: "8*, 14*, 3*", rows: "*" } },
        [
          _c("Label", {
            staticClass: "strong labelInput left",
            attrs: { text: "Superfície", row: "0", col: "0" }
          }),
          _c("Label", {
            staticClass: "labelInput",
            attrs: { text: _vm.superficie, row: "0", col: "1" }
          }),
          _c("Button", {
            attrs: {
              fontSize: "20",
              fontWeight: "800",
              width: "35",
              padding: "0",
              height: "35",
              text: "<",
              row: "0",
              col: "2"
            },
            on: { tap: _vm.openSuperficies }
          })
        ],
        1
      ),
      _c(
        "GridLayout",
        { attrs: { height: "50", columns: "8*, 14*, 3*", rows: "*" } },
        [
          _c("Label", {
            staticClass: "strong labelInput left",
            attrs: { text: "Serviços", row: "0", col: "0" }
          }),
          _c("Button", {
            attrs: {
              fontSize: "20",
              fontWeight: "800",
              padding: "0",
              width: "35",
              height: "35",
              text: "+",
              row: "0",
              col: "2"
            },
            on: { tap: _vm.openServicos }
          })
        ],
        1
      ),
      _c(
        "GridLayout",
        {
          staticClass: "medium",
          attrs: {
            backgroundColor: "#373737",
            height: "30",
            columns: "2*, *, *",
            rows: "*"
          }
        },
        [
          _c("Label", {
            staticClass: "left",
            attrs: { text: "Descrição", row: "0", col: "0" }
          }),
          _c("Label", {
            staticClass: "center",
            attrs: { text: "%", row: "0", col: "1" }
          }),
          _c("Label", {
            staticClass: "right",
            attrs: { text: "Preço", row: "0", col: "2" }
          })
        ],
        1
      ),
      _c(
        "ListView",
        {
          staticStyle: { height: "1000px" },
          attrs: { items: this.servicos_area, "+alias": "i" },
          on: { itemTap: _vm.onItemTap }
        },
        [
          _c("v-template", {
            scopedSlots: _vm._u([
              {
                key: "default",
                fn: function(ref) {
                  var i = ref.i
                  var $index = ref.$index
                  var $even = ref.$even
                  var $odd = ref.$odd
                  return _c(
                    "StackLayout",
                    [
                      _c(
                        "GridLayout",
                        {
                          attrs: {
                            paddingTop: "10",
                            paddingBottom: "10",
                            height: "auto",
                            columns: "2*, *, *",
                            rows: "*"
                          }
                        },
                        [
                          _c("Label", {
                            staticClass: "left",
                            attrs: {
                              textWrap: "true",
                              row: "0",
                              col: "0",
                              text: i.descricao
                            }
                          }),
                          _c("Label", {
                            staticClass: "center",
                            attrs: { row: "0", col: "1", text: i.porcentagem }
                          }),
                          _c("Label", {
                            staticClass: "right",
                            attrs: { row: "0", col: "2", text: i.valor }
                          })
                        ],
                        1
                      ),
                      _c("StackLayout", {
                        attrs: { backgroundColor: "#373737", height: "1" }
                      })
                    ],
                    1
                  )
                }
              }
            ])
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Extras.vue?vue&type=template&id=07c63bf0&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "StackLayout",
    [
      _c(
        "GridLayout",
        { attrs: { height: "30", columns: "8*, 14*, 3*", rows: "*" } },
        [
          _c("Label", {
            staticClass: "strong left",
            attrs: { text: "Valor", row: "0", col: "0" }
          }),
          _c("Label", {
            attrs: { text: _vm.valorTotalExtras, row: "0", col: "1" }
          })
        ],
        1
      ),
      _c(
        "GridLayout",
        { attrs: { height: "50", columns: "12*, 10*, 3*", rows: "*" } },
        [
          _c("Label", {
            staticClass: "strong labelInput left",
            attrs: { text: "Áreas Extras", row: "0", col: "0" }
          }),
          _c("Button", {
            attrs: {
              fontSize: "20",
              fontWeight: "800",
              padding: "0",
              width: "35",
              height: "35",
              text: "+",
              row: "0",
              col: "2"
            },
            on: { tap: _vm.modalAddExtras }
          })
        ],
        1
      ),
      _c("StackLayout", { attrs: { backgroundColor: "#373737", height: "3" } }),
      _c(
        "ListView",
        {
          staticStyle: { height: "900px" },
          attrs: { items: this.extras, "+alias": "i" },
          on: { itemTap: _vm.editAreasExtras }
        },
        [
          _c("v-template", {
            scopedSlots: _vm._u([
              {
                key: "default",
                fn: function(ref) {
                  var i = ref.i
                  var $index = ref.$index
                  var $even = ref.$even
                  var $odd = ref.$odd
                  return _c(
                    "StackLayout",
                    [
                      _c(
                        "StackLayout",
                        [
                          _c("Label", {
                            staticClass: "left strong",
                            attrs: {
                              textWrap: "true",
                              row: "0",
                              col: "0",
                              text: i.nome,
                              padding: "10 0 0 0",
                              fontSize: "20"
                            }
                          }),
                          _c(
                            "GridLayout",
                            {
                              attrs: {
                                paddingTop: "10",
                                paddingBottom: "10",
                                height: "auto",
                                columns: "*, *",
                                rows: "auto"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "left",
                                attrs: {
                                  textWrap: "true",
                                  row: "1",
                                  col: "0",
                                  text: "Valor Total"
                                }
                              }),
                              _c("Label", {
                                staticClass: "right",
                                attrs: {
                                  textWrap: "true",
                                  row: "1",
                                  col: "1",
                                  text: i.valor_total
                                }
                              })
                            ],
                            1
                          ),
                          _c(
                            "GridLayout",
                            {
                              attrs: {
                                paddingTop: "10",
                                paddingBottom: "10",
                                height: "auto",
                                columns: "*, *",
                                rows: "auto"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "strong left",
                                attrs: {
                                  textWrap: "true",
                                  row: "2",
                                  col: "0",
                                  text: "Superficie"
                                }
                              }),
                              _c("Label", {
                                staticClass: "right",
                                attrs: {
                                  textWrap: "true",
                                  row: "2",
                                  col: "1",
                                  text: i.superficie
                                }
                              })
                            ],
                            1
                          ),
                          _c(
                            "GridLayout",
                            {
                              attrs: {
                                paddingTop: "10",
                                paddingBottom: "10",
                                height: "auto",
                                columns: "*, *",
                                rows: "auto"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "strong left",
                                attrs: {
                                  textWrap: "true",
                                  row: "3",
                                  col: "0",
                                  text: "Metragem"
                                }
                              }),
                              _c("Label", {
                                staticClass: "right",
                                attrs: {
                                  textWrap: "true",
                                  row: "3",
                                  col: "1",
                                  text: i.metragem
                                }
                              })
                            ],
                            1
                          ),
                          _c(
                            "GridLayout",
                            {
                              attrs: {
                                paddingTop: "10",
                                paddingBottom: "10",
                                height: "auto",
                                columns: "*, *",
                                rows: "auto"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "strong left",
                                attrs: {
                                  textWrap: "true",
                                  row: "4",
                                  col: "0",
                                  text: "Valor da pintura"
                                }
                              }),
                              _c("Label", {
                                staticClass: "right",
                                attrs: {
                                  textWrap: "true",
                                  row: "4",
                                  col: "1",
                                  text: i.detalhes_valor.valor_pintura
                                }
                              })
                            ],
                            1
                          ),
                          _c(
                            "GridLayout",
                            {
                              attrs: {
                                paddingTop: "10",
                                paddingBottom: "10",
                                height: "auto",
                                columns: "*, *",
                                rows: "auto"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "strong left",
                                attrs: {
                                  textWrap: "true",
                                  row: "5",
                                  col: "0",
                                  text: "Valor Total dos Serviços"
                                }
                              }),
                              _c("Label", {
                                staticClass: "right",
                                attrs: {
                                  textWrap: "true",
                                  row: "5",
                                  col: "1",
                                  text: i.detalhes_valor.valor_servicos
                                }
                              })
                            ],
                            1
                          ),
                          _c(
                            "GridLayout",
                            {
                              staticClass: "medium",
                              attrs: {
                                backgroundColor: "#373737",
                                height: "30",
                                columns: "2*, *, *",
                                rows: "*"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "left",
                                attrs: { text: "Serviços", row: "0", col: "0" }
                              }),
                              _c("Label", {
                                staticClass: "center",
                                attrs: { text: "%", row: "0", col: "1" }
                              }),
                              _c("Label", {
                                staticClass: "right",
                                attrs: { text: "Preço", row: "0", col: "2" }
                              })
                            ],
                            1
                          ),
                          _vm._l(i.servicos, function(q) {
                            return _c(
                              "StackLayout",
                              {
                                attrs: {
                                  row: "6",
                                  col: "0",
                                  paddingTop: "10",
                                  paddingBottom: "10",
                                  height: "auto"
                                }
                              },
                              [
                                _c(
                                  "GridLayout",
                                  {
                                    attrs: {
                                      paddingTop: "5",
                                      paddingBottom: "5",
                                      height: "auto",
                                      columns: "2*, *, *",
                                      rows: "auto"
                                    }
                                  },
                                  [
                                    _c("Label", {
                                      staticClass: "left",
                                      attrs: {
                                        col: "0",
                                        textWrap: "true",
                                        text: q.descricao
                                      }
                                    }),
                                    _c("Label", {
                                      staticClass: "center",
                                      attrs: {
                                        col: "1",
                                        textWrap: "true",
                                        text: q.porcentagem
                                      }
                                    }),
                                    _c("Label", {
                                      staticClass: "right",
                                      attrs: {
                                        col: "2",
                                        textWrap: "true",
                                        text: q.valor
                                      }
                                    })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          })
                        ],
                        2
                      ),
                      _c("StackLayout", {
                        attrs: { backgroundColor: "#373737", height: "1" }
                      })
                    ],
                    1
                  )
                }
              }
            ])
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/MaoDeObra.vue?vue&type=template&id=06c30e89&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "StackLayout",
    [
      _c("Label", { attrs: { text: _vm.type } }),
      _c(
        "StackLayout",
        [
          _c("Label", {
            staticClass: "strong center marginBotton",
            attrs: { text: "Número de Pintores" }
          }),
          _c("Label", {
            staticClass: "center padTop marginBotton marginTop",
            attrs: { text: parseInt(_vm.pintores) }
          }),
          _c("Slider", {
            staticClass: "marg marginTop",
            attrs: { maxValue: "20", value: _vm.pintores },
            on: {
              valueChange: function($event) {
                _vm.pintores = $event.value
              }
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Materiais.vue?vue&type=template&id=3dc6213a&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "StackLayout",
    [
      _c("Button", {
        attrs: {
          fontSize: "20",
          fontWeight: "800",
          padding: "0",
          margin: "0 0 10 0",
          width: "35",
          height: "35",
          text: "+"
        },
        on: { tap: _vm.showAddMaterial }
      }),
      _c(
        "GridLayout",
        {
          staticClass: "medium",
          attrs: {
            backgroundColor: "#373737",
            height: "30",
            columns: "*, *",
            rows: "*"
          }
        },
        [
          _c("Label", {
            staticClass: "left",
            attrs: { text: "Materiais", row: "0", col: "0" }
          }),
          _c("Label", {
            staticClass: "right",
            attrs: { text: "Quantidade", row: "0", col: "1" }
          })
        ],
        1
      ),
      _c(
        "ListView",
        {
          staticStyle: { height: "900" },
          attrs: { items: this.materiais, "+alias": "i" },
          on: { itemTap: _vm.onItemTapMaterial }
        },
        [
          _c("v-template", {
            scopedSlots: _vm._u([
              {
                key: "default",
                fn: function(ref) {
                  var i = ref.i
                  var $index = ref.$index
                  var $even = ref.$even
                  var $odd = ref.$odd
                  return _c(
                    "StackLayout",
                    [
                      _c(
                        "GridLayout",
                        {
                          attrs: {
                            paddingTop: "10",
                            paddingBottom: "10",
                            height: "auto",
                            columns: "*, *",
                            rows: "*"
                          }
                        },
                        [
                          _c("Label", {
                            staticClass: "left",
                            attrs: {
                              textWrap: "true",
                              row: "0",
                              col: "0",
                              text: i.material
                            }
                          }),
                          _c("Label", {
                            staticClass: "right",
                            attrs: { row: "0", col: "1", text: i.quantidade }
                          })
                        ],
                        1
                      ),
                      _c("StackLayout", {
                        attrs: { backgroundColor: "#373737", height: "1" }
                      })
                    ],
                    1
                  )
                }
              }
            ])
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Metragem.vue?vue&type=template&id=7b715e49&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "DockLayout",
    {
      staticClass: "pageModal",
      attrs: { stretchLastChild: "true", backgroundColor: "#707070" }
    },
    [
      _c("ActionBar", {
        attrs: {
          area: _vm.area,
          dock: "top",
          title: "Adicionar Metragem",
          right: " √ "
        },
        on: { addMeters: _vm.addMeters }
      }),
      _c("label", { attrs: { text: this.area } }),
      _c(
        "StackLayout",
        {
          attrs: { backgroundColor: "#262626", height: "100%", dock: "botton" }
        },
        [
          _c(
            "GridLayout",
            {
              staticClass: "marginTop marginBotton",
              attrs: { height: "30", columns: "*, *", rows: "*" }
            },
            [
              _c("Label", {
                staticClass: "strong",
                attrs: { text: "Metragem Total", row: "0", col: "0" }
              }),
              _c("Label", {
                attrs: { text: _vm.totalAreaResult, row: "0", col: "1" }
              })
            ],
            1
          ),
          _c("Button", {
            staticClass: "marginBotton marginTop center strong",
            attrs: {
              fontSize: "20",
              padding: "0",
              width: "35",
              height: "35",
              text: "+",
              row: "0",
              col: "1"
            },
            on: { tap: _vm.modalAddArea }
          }),
          _c(
            "StackLayout",
            {
              staticClass: "medium",
              attrs: { backgroundColor: "#373737", height: "30" }
            },
            [
              _c("Label", {
                staticClass: "center",
                attrs: { text: "Metragens" }
              })
            ],
            1
          ),
          _c(
            "ListView",
            {
              staticStyle: { height: "900" },
              attrs: { items: this.metragens, "+alias": "metragem" },
              on: { itemTap: _vm.onItemTap }
            },
            [
              _c("v-template", {
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(ref) {
                      var metragem = ref.metragem
                      var $index = ref.$index
                      var $even = ref.$even
                      var $odd = ref.$odd
                      return _c(
                        "StackLayout",
                        [
                          _c(
                            "GridLayout",
                            {
                              attrs: {
                                paddingTop: "10",
                                paddingBottom: "10",
                                height: "auto",
                                columns: "*",
                                rows: "*"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "center",
                                attrs: {
                                  textWrap: "true",
                                  row: "0",
                                  col: "0",
                                  text: metragem
                                }
                              })
                            ],
                            1
                          ),
                          _c("StackLayout", {
                            attrs: { backgroundColor: "#373737", height: "1" }
                          })
                        ],
                        1
                      )
                    }
                  }
                ])
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Selecionar.vue?vue&type=template&id=e3d00e34&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    { staticClass: "pageModal", attrs: { backgroundColor: "#707070" } },
    [
      _c(
        "DockLayout",
        { attrs: { stretchLastChild: "true" } },
        [
          _c(
            "StackLayout",
            { attrs: { verticalAlignment: "center" } },
            [
              _c("ActionBar", {
                staticClass: "actionBar",
                attrs: { dock: "top", title: "Selecionar Serviço" }
              }),
              _c(
                "ios",
                [
                  _c("SearchBar", {
                    attrs: { hint: "Filtrar", text: _vm.searchQuery },
                    on: {
                      textChange: function($event) {
                        _vm.searchQuery = $event.value
                      }
                    }
                  })
                ],
                1
              ),
              _c(
                "android",
                [
                  _c("SearchBar", {
                    attrs: {
                      backgroundColor: "white",
                      hint: "Filtrar",
                      text: _vm.searchQuery
                    },
                    on: {
                      textChange: function($event) {
                        _vm.searchQuery = $event.value
                      }
                    }
                  })
                ],
                1
              ),
              _c(
                "ListView",
                {
                  staticClass: "list-group",
                  staticStyle: { height: "1250px" },
                  attrs: {
                    dock: "bottom",
                    items: _vm.listRender,
                    "+alias": "servico"
                  },
                  on: { itemTap: _vm.onItemTap }
                },
                [
                  _c("v-template", {
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function(ref) {
                          var servico = ref.servico
                          var $index = ref.$index
                          var $even = ref.$even
                          var $odd = ref.$odd
                          return _c(
                            "FlexboxLayout",
                            {
                              staticClass: "list-group-item",
                              attrs: { flexDirection: "row" }
                            },
                            [
                              _c("Label", {
                                staticClass: "list-group-item-heading",
                                staticStyle: { width: "60%" },
                                attrs: { textWrap: "true", text: servico }
                              })
                            ],
                            1
                          )
                        }
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Servicos.vue?vue&type=template&id=e220d50e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    { staticClass: "page", attrs: { backgroundColor: "#262626" } },
    [
      _c(
        "StackLayout",
        {
          staticClass: "marginSides title",
          attrs: { verticalAlignment: "center" }
        },
        [
          _c(
            "FlexboxLayout",
            { attrs: { height: "40", flexDirection: "row" } },
            [
              _c("Label", {
                staticClass: "closeModal",
                attrs: { text: "X", width: "100%" },
                on: { tap: _vm.modalClose }
              })
            ],
            1
          ),
          _c(
            "StackLayout",
            { staticClass: "center", attrs: { verticalAlignment: "center" } },
            [
              _c("Label", {
                staticClass: "strong title marginBotton",
                attrs: { textWrap: "true", text: "Adicionar Serviço:" }
              }),
              _c("Label", {
                staticClass: "marginBotton",
                attrs: {
                  backgroundColor: "transparent",
                  color: "#1A75FE",
                  text: "Selecionar Serviço",
                  visibility: _vm.selectServiceButton
                },
                on: { tap: _vm.selectService }
              }),
              _c("Label", {
                staticClass: "marginBotton",
                attrs: {
                  backgroundColor: "transparent",
                  textWrap: "true",
                  fontSize: "14",
                  horizontalAlignment: "center",
                  text: this.selectedItem,
                  visibility: _vm.selectedServiceButton
                },
                on: { tap: _vm.selectService }
              }),
              _c("Label", {
                staticClass: "medium padTop marginBotton",
                attrs: { text: "Porcentagem do valor total" }
              }),
              _c("Label", {
                staticClass: "center padTop marginBotton",
                attrs: { text: _vm.porcentagemSlider + " %" }
              }),
              _c("Slider", {
                staticClass: "marg marginTop padTop",
                attrs: {
                  minValue: "1",
                  maxValue: "100",
                  value: _vm.porcentagemSlider
                },
                on: {
                  valueChange: function($event) {
                    _vm.porcentagemSlider = $event.value
                  }
                }
              }),
              _c("Label", {
                staticClass: "strong padTop",
                attrs: { text: "Valor do Serviço" }
              }),
              _c("Label", {
                staticClass: "labelCalculadora",
                attrs: { text: "R$ " + _vm.valor }
              }),
              _c("Button", {
                staticClass: "margTopLess",
                attrs: { visibility: _vm.addControls, text: "Adicionar" },
                on: { tap: _vm.adicionarServico }
              }),
              _c(
                "GridLayout",
                {
                  attrs: {
                    visibility: _vm.editControls,
                    height: "80",
                    width: "100%",
                    rows: "*",
                    columns: "5*, *, 5*"
                  }
                },
                [
                  _c("Button", {
                    staticClass: "margTopLess",
                    attrs: {
                      row: "0",
                      col: "0",
                      backgroundColor: "#D12020",
                      text: "Excluir"
                    },
                    on: { tap: _vm.removerServico }
                  }),
                  _c("Button", {
                    staticClass: "margTopLess",
                    attrs: {
                      row: "0",
                      col: "2",
                      backgroundColor: "#0EA174",
                      text: "Salvar"
                    },
                    on: { tap: _vm.salvarServico }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Superficies.vue?vue&type=template&id=e47e1d86&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    { staticClass: "page", attrs: { backgroundColor: "#262626" } },
    [
      _c(
        "StackLayout",
        { staticClass: "marginSides verticalCenter" },
        [
          _c(
            "FlexboxLayout",
            { attrs: { height: "40", flexDirection: "row" } },
            [
              _c("Label", {
                staticClass: "closeModal",
                attrs: { text: "X", width: "100%" },
                on: { tap: _vm.modalClose }
              })
            ],
            1
          ),
          _c(
            "StackLayout",
            [
              _c("Label", {
                staticClass: "center strong title",
                attrs: {
                  textWrap: "true",
                  text: "Selecione o tipo de Superfície"
                }
              }),
              _c("ListPicker", {
                attrs: {
                  marginBottom: "15",
                  items: this.superficies,
                  textField: "nome",
                  valueField: "fator_multiplicador",
                  selectedIndex: _vm.selectedSuperficieIndex
                },
                on: {
                  selectedIndexChange: function($event) {
                    _vm.selectedSuperficieIndex = $event.value
                  }
                }
              }),
              _c("Button", {
                attrs: { text: "Selecionar" },
                on: { tap: _vm.selecionarSuperficie }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Login.vue?vue&type=template&id=c27482c4&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    {
      attrs: { backgroundSpanUnderStatusBar: "true", actionBarHidden: "true" },
      on: { loaded: _vm.pageLoaded }
    },
    [
      _c(
        "FlexboxLayout",
        { staticClass: "page" },
        [
          _c(
            "StackLayout",
            { staticClass: "form" },
            [
              _c("Image", {
                staticClass: "logo",
                attrs: { src: "~/images/logo.png" }
              }),
              _c(
                "GridLayout",
                { attrs: { rows: "auto, auto" } },
                [
                  _c(
                    "StackLayout",
                    { staticClass: "input-field", attrs: { row: "0" } },
                    [
                      _c("TextField", {
                        staticClass: "input marginBotton",
                        attrs: {
                          hint: "Email",
                          keyboardType: "email",
                          returnKeyType: "Próximo",
                          text: _vm.user_mail
                        },
                        on: {
                          textChange: function($event) {
                            _vm.user_mail = $event.value
                          }
                        }
                      }),
                      _c("StackLayout", { staticClass: "hr-light" })
                    ],
                    1
                  ),
                  _c(
                    "StackLayout",
                    { staticClass: "input-field", attrs: { row: "1" } },
                    [
                      _c("TextField", {
                        staticClass: "input marginBotton",
                        attrs: {
                          secure: "true",
                          hint: "Senha",
                          returnKeyType: "Ok",
                          text: _vm.user_password
                        },
                        on: {
                          textChange: function($event) {
                            _vm.user_password = $event.value
                          }
                        }
                      }),
                      _c("StackLayout", { staticClass: "hr-light" })
                    ],
                    1
                  ),
                  _c("ActivityIndicator", { attrs: { rowSpan: "3" } })
                ],
                1
              ),
              _c("Button", { attrs: { text: "Login" }, on: { tap: _vm.login } })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Pedidos.vue?vue&type=template&id=6dda1343&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    { attrs: { actionBarHidden: "true" } },
    [
      _c(
        "DockLayout",
        { attrs: { stretchLastChild: "true" } },
        [
          _c("ActionBar", {
            attrs: { dock: "top", left: "Logout", title: "Pedidos" }
          }),
          _c(
            "ListView",
            {
              staticStyle: { height: "800" },
              attrs: {
                dock: "bottom",
                items: this.$store.state.pedidos,
                "+alias": "pedido"
              },
              on: { itemTap: _vm.toCalculador }
            },
            [
              _c("v-template", {
                staticClass: "page",
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(ref) {
                      var pedido = ref.pedido
                      var $index = ref.$index
                      var $even = ref.$even
                      var $odd = ref.$odd
                      return _c(
                        "StackLayout",
                        { staticClass: "page" },
                        [
                          _c(
                            "GridLayout",
                            {
                              staticClass: "page",
                              attrs: {
                                textWrap: "true",
                                columns: "*, *, *, *",
                                rows: "*, *, *, *, *"
                              }
                            },
                            [
                              _c("Label", {
                                staticClass: "title medium",
                                attrs: {
                                  fontSize: "18",
                                  padding: "5 0 0 0",
                                  text: pedido.condominio.nome,
                                  row: "0",
                                  col: "0",
                                  colSpan: "4"
                                }
                              }),
                              _c("Label", {
                                attrs: {
                                  fontSize: "15",
                                  padding: "5 0",
                                  text:
                                    pedido.condominio.endereco.logradouro +
                                    ", " +
                                    pedido.condominio.endereco.numero,
                                  row: "1",
                                  col: "0",
                                  colSpan: "2"
                                }
                              }),
                              _c("Label", {
                                attrs: {
                                  fontSize: "15",
                                  padding: "5 0",
                                  text:
                                    "CEP: " + pedido.condominio.endereco.CEP,
                                  row: "1",
                                  col: "2",
                                  colSpan: "2"
                                }
                              }),
                              _c("Label", {
                                attrs: {
                                  fontSize: "15",
                                  padding: "10 0",
                                  text:
                                    "Número de prédios: " +
                                    pedido.condominio.numero_de_predios,
                                  row: "3",
                                  col: "0",
                                  textWrap: "true",
                                  colSpan: "2"
                                }
                              }),
                              _c("Label", {
                                attrs: {
                                  fontSize: "15",
                                  padding: "10 0",
                                  text:
                                    "Número de andares: " +
                                    pedido.condominio
                                      .numero_de_andares_por_predio,
                                  row: "3",
                                  col: "2",
                                  textWrap: "true",
                                  colSpan: "2"
                                }
                              }),
                              _c("Label", {
                                staticClass: "title medium",
                                attrs: {
                                  fontSize: "15",
                                  padding: "10 0",
                                  text: pedido.condominio.sindico.nome,
                                  row: "4",
                                  col: "0",
                                  colSpan: "2"
                                }
                              }),
                              _c("Label", {
                                staticClass: "title medium",
                                attrs: {
                                  fontSize: "15",
                                  padding: "10 0",
                                  text:
                                    "Contato: " +
                                    pedido.condominio.sindico.celular_1,
                                  row: "4",
                                  col: "2",
                                  colSpan: "2"
                                }
                              })
                            ],
                            1
                          ),
                          _c("StackLayout", {
                            attrs: { backgroundColor: "#373737", height: "1" }
                          })
                        ],
                        1
                      )
                    }
                  }
                ])
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/ResetSenha.vue?vue&type=template&id=1105423e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    [
      _c(
        "FlexboxLayout",
        { staticClass: "page" },
        [
          _c(
            "StackLayout",
            { staticClass: "form" },
            [
              _c(
                "FlexboxLayout",
                { attrs: { flexDirection: "row" } },
                [
                  _c("Label", {
                    staticClass: "closeModal",
                    attrs: { text: "X", width: "100%" },
                    on: { tap: _vm.closeModal }
                  })
                ],
                1
              ),
              _c(
                "StackLayout",
                { staticClass: "modalView" },
                [
                  _c("Label", {
                    staticClass: "marginBotton strong title",
                    attrs: { textWrap: "true", text: "Redefinir senha" }
                  }),
                  _c("Label", {
                    attrs: {
                      textWrap: "true",
                      text: "Informe seu e-mail para resetar sua senha."
                    }
                  }),
                  _c("TextField", {
                    staticClass: "input",
                    attrs: { hint: "Email" }
                  }),
                  _c("StackLayout", { staticClass: "hr-light marginBotton" }),
                  _c("Button", {
                    attrs: { text: "Enviar" },
                    on: { tap: _vm.closeModal }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Resumo.vue?vue&type=template&id=f7d49c5c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    [
      _c(
        "android",
        [
          _c(
            "ActionBar",
            { attrs: { title: "Resumo" } },
            [
              _c("NavigationButton", {
                attrs: { "android.systemIcon": "ic_menu_back" },
                on: { tap: _vm.$navigateBack }
              })
            ],
            1
          )
        ],
        1
      ),
      _c(
        "ios",
        [
          _c(
            "ActionBar",
            { staticClass: "actionBar", attrs: { title: "Resumo" } },
            [
              _c("NavigationButton", {
                attrs: {
                  text: "Orçamento",
                  "android.systemIcon": "ic_menu_back"
                },
                on: { tap: _vm.$navigateBack }
              })
            ],
            1
          )
        ],
        1
      ),
      _c(
        "FlexboxLayout",
        { staticClass: "page" },
        [
          _c(
            "StackLayout",
            [
              _c("Label", {
                attrs: {
                  textWrap: "true",
                  text: JSON.stringify(this.$store.state.pedido.id_pedido)
                }
              }),
              _c("StackLayout", { staticClass: "hr-light" }),
              _c("Label", {
                attrs: {
                  textWrap: "true",
                  text: JSON.stringify(this.$store.state.pedido.valor_total)
                }
              }),
              _c("StackLayout", { staticClass: "hr-light" }),
              _c("Label", {
                attrs: {
                  textWrap: "true",
                  text: JSON.stringify(
                    this.$store.state.pedido.orcamento.orcamento_area_externa
                  )
                }
              }),
              _c("StackLayout", { staticClass: "hr-light" }),
              _c("Label", {
                attrs: {
                  textWrap: "true",
                  text: JSON.stringify(
                    this.$store.state.pedido.orcamento.orcamento_area_interna
                  )
                }
              }),
              _c("StackLayout", { staticClass: "hr-light" }),
              _c("Label", {
                attrs: {
                  textWrap: "true",
                  text: JSON.stringify(
                    this.$store.state.pedido.orcamento.orcamento_area_extra
                  )
                }
              }),
              _c("StackLayout", { staticClass: "hr-light" }),
              _c("Label", {
                attrs: {
                  textWrap: "true",
                  text: JSON.stringify(
                    this.$store.state.pedido.orcamento.materiais
                  )
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/shared/ActionBar.vue?vue&type=template&id=2618786e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "StackLayout",
    [
      _c(
        "GridLayout",
        {
          staticClass: "actionBar",
          attrs: { columns: "*, 3*, *", rows: "*", height: "34" }
        },
        [
          _c("Label", {
            staticClass: "left strong",
            attrs: {
              fontSize: "16",
              text: _vm.left,
              paddingLeft: "5",
              col: "0",
              row: "0"
            },
            on: { tap: _vm.leftFunction }
          }),
          _c("Label", {
            attrs: {
              fontWeight: "400",
              fontSize: "16",
              text: _vm.title,
              col: "1",
              row: "0"
            }
          }),
          _c("Label", {
            staticClass: "right strong",
            attrs: {
              fontSize: "16",
              paddingRight: "5",
              text: _vm.right,
              col: "2",
              row: "0"
            },
            on: { tap: _vm.rightFunction }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./ sync ^\\.\\/app\\.(css|scss|less|sass)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app.scss": "./app.scss"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync ^\\.\\/app\\.(css|scss|less|sass)$";

/***/ }),

/***/ "./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	var e = new Error("Cannot find module '" + req + "'");
	e.code = 'MODULE_NOT_FOUND';
	throw e;
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$";

/***/ }),

/***/ "./app.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Convenient single import for dark variables\n * Includes base variables with dark overrides\n **/\n/**\n * Dark variable overrides\n **/\n/**\n * Color classes\n * The following creates this pattern:\n * .c-grey{color:#e0e0e0}.c-bg-grey{background-color:#e0e0e0}\n**/\n.c-white {\n  color: #fff; }\n\n.c-bg-white {\n  background-color: #fff; }\n\n.c-black {\n  color: #000; }\n\n.c-bg-black {\n  background-color: #000; }\n\n.c-aqua {\n  color: #00caab; }\n\n.c-bg-aqua {\n  background-color: #00caab; }\n\n.c-blue {\n  color: #3d5afe; }\n\n.c-bg-blue {\n  background-color: #3d5afe; }\n\n.c-charcoal {\n  color: #303030; }\n\n.c-bg-charcoal {\n  background-color: #303030; }\n\n.c-brown {\n  color: #795548; }\n\n.c-bg-brown {\n  background-color: #795548; }\n\n.c-forest {\n  color: #006968; }\n\n.c-bg-forest {\n  background-color: #006968; }\n\n.c-grey {\n  color: #e0e0e0; }\n\n.c-bg-grey {\n  background-color: #e0e0e0; }\n\n.c-grey-light {\n  color: #bababa; }\n\n.c-bg-grey-light {\n  background-color: #bababa; }\n\n.c-grey-dark {\n  color: #5c687c; }\n\n.c-bg-grey-dark {\n  background-color: #5c687c; }\n\n.c-purple {\n  color: #8130ff; }\n\n.c-bg-purple {\n  background-color: #8130ff; }\n\n.c-lemon {\n  color: #ffea00; }\n\n.c-bg-lemon {\n  background-color: #ffea00; }\n\n.c-lime {\n  color: #aee406; }\n\n.c-bg-lime {\n  background-color: #aee406; }\n\n.c-orange {\n  color: #f57c00; }\n\n.c-bg-orange {\n  background-color: #f57c00; }\n\n.c-ruby {\n  color: #ff1744; }\n\n.c-bg-ruby {\n  background-color: #ff1744; }\n\n.c-sky {\n  color: #30bcff; }\n\n.c-bg-sky {\n  background-color: #30bcff; }\n\n/* Width/Height */\n.w-full {\n  width: 100%; }\n\n.w-100 {\n  width: 100; }\n\n.h-full {\n  height: 100%; }\n\n.h-100 {\n  height: 100; }\n\n/**\n * Margin and Padding\n * The following creates this pattern:\n * .m-0{margin:0}.m-t-0{margin-top:0}.m-r-0{margin-right:0}.m-b-0{margin-bottom:0}.m-l-0{margin-left:0}.m-x-0{margin-right:0;margin-left:0}.m-y-0{margin-top:0;margin-bottom:0}\n * Same for Padding (using the 'p' abbreviation)\n * From 0, 2, 5, 10, 15, 20, 25, 30\n**/\n.m-0 {\n  margin: 0; }\n\n.m-t-0 {\n  margin-top: 0; }\n\n.m-r-0 {\n  margin-right: 0; }\n\n.m-b-0 {\n  margin-bottom: 0; }\n\n.m-l-0 {\n  margin-left: 0; }\n\n.m-x-0 {\n  margin-right: 0;\n  margin-left: 0; }\n\n.m-y-0 {\n  margin-top: 0;\n  margin-bottom: 0; }\n\n.m-2 {\n  margin: 2; }\n\n.m-t-2 {\n  margin-top: 2; }\n\n.m-r-2 {\n  margin-right: 2; }\n\n.m-b-2 {\n  margin-bottom: 2; }\n\n.m-l-2 {\n  margin-left: 2; }\n\n.m-x-2 {\n  margin-right: 2;\n  margin-left: 2; }\n\n.m-y-2 {\n  margin-top: 2;\n  margin-bottom: 2; }\n\n.m-4 {\n  margin: 4; }\n\n.m-t-4 {\n  margin-top: 4; }\n\n.m-r-4 {\n  margin-right: 4; }\n\n.m-b-4 {\n  margin-bottom: 4; }\n\n.m-l-4 {\n  margin-left: 4; }\n\n.m-x-4 {\n  margin-right: 4;\n  margin-left: 4; }\n\n.m-y-4 {\n  margin-top: 4;\n  margin-bottom: 4; }\n\n.m-5 {\n  margin: 5; }\n\n.m-t-5 {\n  margin-top: 5; }\n\n.m-r-5 {\n  margin-right: 5; }\n\n.m-b-5 {\n  margin-bottom: 5; }\n\n.m-l-5 {\n  margin-left: 5; }\n\n.m-x-5 {\n  margin-right: 5;\n  margin-left: 5; }\n\n.m-y-5 {\n  margin-top: 5;\n  margin-bottom: 5; }\n\n.m-8 {\n  margin: 8; }\n\n.m-t-8 {\n  margin-top: 8; }\n\n.m-r-8 {\n  margin-right: 8; }\n\n.m-b-8 {\n  margin-bottom: 8; }\n\n.m-l-8 {\n  margin-left: 8; }\n\n.m-x-8 {\n  margin-right: 8;\n  margin-left: 8; }\n\n.m-y-8 {\n  margin-top: 8;\n  margin-bottom: 8; }\n\n.m-10 {\n  margin: 10; }\n\n.m-t-10 {\n  margin-top: 10; }\n\n.m-r-10 {\n  margin-right: 10; }\n\n.m-b-10 {\n  margin-bottom: 10; }\n\n.m-l-10 {\n  margin-left: 10; }\n\n.m-x-10 {\n  margin-right: 10;\n  margin-left: 10; }\n\n.m-y-10 {\n  margin-top: 10;\n  margin-bottom: 10; }\n\n.m-12 {\n  margin: 12; }\n\n.m-t-12 {\n  margin-top: 12; }\n\n.m-r-12 {\n  margin-right: 12; }\n\n.m-b-12 {\n  margin-bottom: 12; }\n\n.m-l-12 {\n  margin-left: 12; }\n\n.m-x-12 {\n  margin-right: 12;\n  margin-left: 12; }\n\n.m-y-12 {\n  margin-top: 12;\n  margin-bottom: 12; }\n\n.m-15 {\n  margin: 15; }\n\n.m-t-15 {\n  margin-top: 15; }\n\n.m-r-15 {\n  margin-right: 15; }\n\n.m-b-15 {\n  margin-bottom: 15; }\n\n.m-l-15 {\n  margin-left: 15; }\n\n.m-x-15 {\n  margin-right: 15;\n  margin-left: 15; }\n\n.m-y-15 {\n  margin-top: 15;\n  margin-bottom: 15; }\n\n.m-16 {\n  margin: 16; }\n\n.m-t-16 {\n  margin-top: 16; }\n\n.m-r-16 {\n  margin-right: 16; }\n\n.m-b-16 {\n  margin-bottom: 16; }\n\n.m-l-16 {\n  margin-left: 16; }\n\n.m-x-16 {\n  margin-right: 16;\n  margin-left: 16; }\n\n.m-y-16 {\n  margin-top: 16;\n  margin-bottom: 16; }\n\n.m-20 {\n  margin: 20; }\n\n.m-t-20 {\n  margin-top: 20; }\n\n.m-r-20 {\n  margin-right: 20; }\n\n.m-b-20 {\n  margin-bottom: 20; }\n\n.m-l-20 {\n  margin-left: 20; }\n\n.m-x-20 {\n  margin-right: 20;\n  margin-left: 20; }\n\n.m-y-20 {\n  margin-top: 20;\n  margin-bottom: 20; }\n\n.m-24 {\n  margin: 24; }\n\n.m-t-24 {\n  margin-top: 24; }\n\n.m-r-24 {\n  margin-right: 24; }\n\n.m-b-24 {\n  margin-bottom: 24; }\n\n.m-l-24 {\n  margin-left: 24; }\n\n.m-x-24 {\n  margin-right: 24;\n  margin-left: 24; }\n\n.m-y-24 {\n  margin-top: 24;\n  margin-bottom: 24; }\n\n.m-25 {\n  margin: 25; }\n\n.m-t-25 {\n  margin-top: 25; }\n\n.m-r-25 {\n  margin-right: 25; }\n\n.m-b-25 {\n  margin-bottom: 25; }\n\n.m-l-25 {\n  margin-left: 25; }\n\n.m-x-25 {\n  margin-right: 25;\n  margin-left: 25; }\n\n.m-y-25 {\n  margin-top: 25;\n  margin-bottom: 25; }\n\n.m-28 {\n  margin: 28; }\n\n.m-t-28 {\n  margin-top: 28; }\n\n.m-r-28 {\n  margin-right: 28; }\n\n.m-b-28 {\n  margin-bottom: 28; }\n\n.m-l-28 {\n  margin-left: 28; }\n\n.m-x-28 {\n  margin-right: 28;\n  margin-left: 28; }\n\n.m-y-28 {\n  margin-top: 28;\n  margin-bottom: 28; }\n\n.m-30 {\n  margin: 30; }\n\n.m-t-30 {\n  margin-top: 30; }\n\n.m-r-30 {\n  margin-right: 30; }\n\n.m-b-30 {\n  margin-bottom: 30; }\n\n.m-l-30 {\n  margin-left: 30; }\n\n.m-x-30 {\n  margin-right: 30;\n  margin-left: 30; }\n\n.m-y-30 {\n  margin-top: 30;\n  margin-bottom: 30; }\n\n.p-0 {\n  padding: 0; }\n\n.p-t-0 {\n  padding-top: 0; }\n\n.p-r-0 {\n  padding-right: 0; }\n\n.p-b-0 {\n  padding-bottom: 0; }\n\n.p-l-0 {\n  padding-left: 0; }\n\n.p-x-0 {\n  padding-right: 0;\n  padding-left: 0; }\n\n.p-y-0 {\n  padding-top: 0;\n  padding-bottom: 0; }\n\n.p-2 {\n  padding: 2; }\n\n.p-t-2 {\n  padding-top: 2; }\n\n.p-r-2 {\n  padding-right: 2; }\n\n.p-b-2 {\n  padding-bottom: 2; }\n\n.p-l-2 {\n  padding-left: 2; }\n\n.p-x-2 {\n  padding-right: 2;\n  padding-left: 2; }\n\n.p-y-2 {\n  padding-top: 2;\n  padding-bottom: 2; }\n\n.p-4 {\n  padding: 4; }\n\n.p-t-4 {\n  padding-top: 4; }\n\n.p-r-4 {\n  padding-right: 4; }\n\n.p-b-4 {\n  padding-bottom: 4; }\n\n.p-l-4 {\n  padding-left: 4; }\n\n.p-x-4 {\n  padding-right: 4;\n  padding-left: 4; }\n\n.p-y-4 {\n  padding-top: 4;\n  padding-bottom: 4; }\n\n.p-5 {\n  padding: 5; }\n\n.p-t-5 {\n  padding-top: 5; }\n\n.p-r-5 {\n  padding-right: 5; }\n\n.p-b-5 {\n  padding-bottom: 5; }\n\n.p-l-5 {\n  padding-left: 5; }\n\n.p-x-5 {\n  padding-right: 5;\n  padding-left: 5; }\n\n.p-y-5 {\n  padding-top: 5;\n  padding-bottom: 5; }\n\n.p-8 {\n  padding: 8; }\n\n.p-t-8 {\n  padding-top: 8; }\n\n.p-r-8 {\n  padding-right: 8; }\n\n.p-b-8 {\n  padding-bottom: 8; }\n\n.p-l-8 {\n  padding-left: 8; }\n\n.p-x-8 {\n  padding-right: 8;\n  padding-left: 8; }\n\n.p-y-8 {\n  padding-top: 8;\n  padding-bottom: 8; }\n\n.p-10 {\n  padding: 10; }\n\n.p-t-10 {\n  padding-top: 10; }\n\n.p-r-10 {\n  padding-right: 10; }\n\n.p-b-10 {\n  padding-bottom: 10; }\n\n.p-l-10 {\n  padding-left: 10; }\n\n.p-x-10 {\n  padding-right: 10;\n  padding-left: 10; }\n\n.p-y-10 {\n  padding-top: 10;\n  padding-bottom: 10; }\n\n.p-12 {\n  padding: 12; }\n\n.p-t-12 {\n  padding-top: 12; }\n\n.p-r-12 {\n  padding-right: 12; }\n\n.p-b-12 {\n  padding-bottom: 12; }\n\n.p-l-12 {\n  padding-left: 12; }\n\n.p-x-12 {\n  padding-right: 12;\n  padding-left: 12; }\n\n.p-y-12 {\n  padding-top: 12;\n  padding-bottom: 12; }\n\n.p-15 {\n  padding: 15; }\n\n.p-t-15 {\n  padding-top: 15; }\n\n.p-r-15 {\n  padding-right: 15; }\n\n.p-b-15 {\n  padding-bottom: 15; }\n\n.p-l-15 {\n  padding-left: 15; }\n\n.p-x-15 {\n  padding-right: 15;\n  padding-left: 15; }\n\n.p-y-15 {\n  padding-top: 15;\n  padding-bottom: 15; }\n\n.p-16 {\n  padding: 16; }\n\n.p-t-16 {\n  padding-top: 16; }\n\n.p-r-16 {\n  padding-right: 16; }\n\n.p-b-16 {\n  padding-bottom: 16; }\n\n.p-l-16 {\n  padding-left: 16; }\n\n.p-x-16 {\n  padding-right: 16;\n  padding-left: 16; }\n\n.p-y-16 {\n  padding-top: 16;\n  padding-bottom: 16; }\n\n.p-20 {\n  padding: 20; }\n\n.p-t-20 {\n  padding-top: 20; }\n\n.p-r-20 {\n  padding-right: 20; }\n\n.p-b-20 {\n  padding-bottom: 20; }\n\n.p-l-20 {\n  padding-left: 20; }\n\n.p-x-20 {\n  padding-right: 20;\n  padding-left: 20; }\n\n.p-y-20 {\n  padding-top: 20;\n  padding-bottom: 20; }\n\n.p-24 {\n  padding: 24; }\n\n.p-t-24 {\n  padding-top: 24; }\n\n.p-r-24 {\n  padding-right: 24; }\n\n.p-b-24 {\n  padding-bottom: 24; }\n\n.p-l-24 {\n  padding-left: 24; }\n\n.p-x-24 {\n  padding-right: 24;\n  padding-left: 24; }\n\n.p-y-24 {\n  padding-top: 24;\n  padding-bottom: 24; }\n\n.p-25 {\n  padding: 25; }\n\n.p-t-25 {\n  padding-top: 25; }\n\n.p-r-25 {\n  padding-right: 25; }\n\n.p-b-25 {\n  padding-bottom: 25; }\n\n.p-l-25 {\n  padding-left: 25; }\n\n.p-x-25 {\n  padding-right: 25;\n  padding-left: 25; }\n\n.p-y-25 {\n  padding-top: 25;\n  padding-bottom: 25; }\n\n.p-28 {\n  padding: 28; }\n\n.p-t-28 {\n  padding-top: 28; }\n\n.p-r-28 {\n  padding-right: 28; }\n\n.p-b-28 {\n  padding-bottom: 28; }\n\n.p-l-28 {\n  padding-left: 28; }\n\n.p-x-28 {\n  padding-right: 28;\n  padding-left: 28; }\n\n.p-y-28 {\n  padding-top: 28;\n  padding-bottom: 28; }\n\n.p-30 {\n  padding: 30; }\n\n.p-t-30 {\n  padding-top: 30; }\n\n.p-r-30 {\n  padding-right: 30; }\n\n.p-b-30 {\n  padding-bottom: 30; }\n\n.p-l-30 {\n  padding-left: 30; }\n\n.p-x-30 {\n  padding-right: 30;\n  padding-left: 30; }\n\n.p-y-30 {\n  padding-top: 30;\n  padding-bottom: 30; }\n\n/* Dividers */\n.hr-light {\n  height: 1;\n  background-color: #e0e0e0;\n  width: 100%; }\n\n.hr-dark {\n  height: 1;\n  background-color: #303030;\n  width: 100%; }\n\n/* Alignment */\n.text-left {\n  text-align: left; }\n\n.text-right {\n  text-align: right; }\n\n.text-center {\n  text-align: center; }\n\n.text-lowercase {\n  text-transform: lowercase; }\n\n.text-uppercase {\n  text-transform: uppercase; }\n\n.text-capitalize {\n  text-transform: capitalize; }\n\n.font-weight-normal {\n  font-weight: normal; }\n\n.font-weight-bold {\n  font-weight: bold; }\n\n.font-italic {\n  font-style: italic; }\n\n/**\n * Font size\n * The following creates this pattern:\n * .t-10{font-size:10}\n * From 10, 12, 14, 15, 16, 17, 18, 19, 20\n**/\n.t-10 {\n  font-size: 10; }\n\n.t-12 {\n  font-size: 12; }\n\n.t-14 {\n  font-size: 14; }\n\n.t-15 {\n  font-size: 15; }\n\n.t-16 {\n  font-size: 16; }\n\n.t-17 {\n  font-size: 17; }\n\n.t-18 {\n  font-size: 18; }\n\n.t-19 {\n  font-size: 19; }\n\n.t-20 {\n  font-size: 20; }\n\n.t-25 {\n  font-size: 25; }\n\n.t-30 {\n  font-size: 30; }\n\n.img-rounded {\n  border-radius: 5; }\n\n.img-circle {\n  border-radius: 20; }\n\n.img-thumbnail {\n  border-radius: 0; }\n\n.invisible {\n  visibility: collapse; }\n\n.pull-left {\n  horizontal-align: left; }\n\n.pull-right {\n  horizontal-align: right; }\n\n.m-x-auto {\n  horizontal-align: center; }\n\n.m-y-auto {\n  vertical-align: center; }\n\n.text-primary {\n  color: #30bcff; }\n\n.text-danger {\n  color: #d50000; }\n\n.text-muted {\n  color: gray; }\n\n.bg-primary {\n  background-color: #30bcff;\n  color: #fff; }\n\n.bg-danger {\n  background-color: #d50000;\n  color: #fff; }\n\n.action-bar {\n  background-color: #303030;\n  color: #fff; }\n  .action-bar .action-bar-title {\n    font-weight: bold;\n    font-size: 17;\n    vertical-align: center; }\n  .action-bar .action-item {\n    font-weight: normal; }\n\n.activity-indicator {\n  color: #30bcff;\n  width: 30;\n  height: 30; }\n\n.btn {\n  color: #30bcff;\n  background-color: transparent;\n  min-height: 36;\n  min-width: 64;\n  padding: 10 10 10 10;\n  font-size: 18;\n  margin: 8 16 8 16; }\n  .btn.btn-active:highlighted {\n    color: #fff;\n    background-color: #c0ebff; }\n\n.btn-primary {\n  background-color: #30bcff;\n  border-color: #30bcff;\n  color: #fff; }\n  .btn-primary.btn-active:highlighted {\n    background-color: #01a0ec;\n    border-color: #01a0ec; }\n  .btn-primary.btn-aqua {\n    background-color: #00caab; }\n  .btn-primary.btn-blue {\n    background-color: #3d5afe; }\n  .btn-primary.btn-brown {\n    background-color: #795548; }\n  .btn-primary.btn-forest {\n    background-color: #006968; }\n  .btn-primary.btn-grey {\n    background-color: #5c687c; }\n  .btn-primary.btn-lemon {\n    background-color: #ffea00;\n    color: #000; }\n  .btn-primary.btn-lime {\n    background-color: #aee406;\n    color: #000; }\n  .btn-primary.btn-orange {\n    background-color: #f57c00; }\n  .btn-primary.btn-purple {\n    background-color: #8130ff; }\n  .btn-primary.btn-ruby {\n    background-color: #ff1744; }\n  .btn-primary.btn-sky {\n    background-color: #30bcff; }\n\n.btn-outline {\n  background-color: transparent;\n  border-color: #30bcff;\n  color: #30bcff; }\n  .btn-outline.btn-active:highlighted {\n    background-color: #c0ebff; }\n\n.btn[isEnabled=false] {\n  color: #a4a4a4;\n  background-color: #e0e0e0;\n  border-color: #e0e0e0; }\n\n.fa {\n  font-family: FontAwesome, fontawesome-webfont; }\n\n.form .input {\n  padding: 16 8 16 8;\n  background-color: transparent; }\n  .form .input.input-border {\n    border-width: 1;\n    border-color: #e0e0e0;\n    border-radius: 2;\n    padding: 16; }\n  .form .input.input-rounded {\n    border-width: 1;\n    border-color: #e0e0e0;\n    border-radius: 28;\n    padding: 16; }\n  .form .input[isEnabled='false'] {\n    background-color: #fafafa; }\n\n.form .input-field {\n  margin: 8; }\n  .form .input-field .label {\n    font-size: 12;\n    color: #bababa; }\n  .form .input-field .input {\n    padding: 0;\n    margin: 0 0 8 0; }\n  .form .input-field .hr-light.active,\n  .form .input-field .hr-dark.active {\n    background-color: #30bcff; }\n  .form .input-field.input-sides .label {\n    font-size: 18;\n    margin: 0 0 8 0; }\n\n.h1, .h2, .h3, .h4, .h5, .h6 {\n  margin-bottom: 4;\n  font-weight: normal;\n  color: #fff; }\n\n.body,\n.body2,\n.footnote {\n  font-weight: normal;\n  color: #b3b3b3; }\n\n.h1 {\n  font-size: 32; }\n\n.h2 {\n  font-size: 22; }\n\n.h3 {\n  font-size: 15; }\n\n.h4 {\n  font-size: 12; }\n\n.h5 {\n  font-size: 11; }\n\n.h6 {\n  font-size: 10; }\n\n.body {\n  font-size: 14; }\n\n.body2 {\n  font-size: 17; }\n\n.footnote {\n  font-size: 13; }\n\n.list-group .list-group-item {\n  color: #fff;\n  font-size: 16;\n  margin: 0;\n  padding: 16; }\n  .list-group .list-group-item Label {\n    vertical-align: center; }\n  .list-group .list-group-item .thumb {\n    stretch: fill;\n    width: 40;\n    height: 40;\n    margin-right: 16; }\n  .list-group .list-group-item.active {\n    background-color: #e0e0e0; }\n  .list-group .list-group-item .list-group-item-text {\n    color: #b3b3b3;\n    font-size: 14; }\n\n.page {\n  background-color: #303030; }\n\n.progress {\n  color: #30bcff;\n  background-color: #bababa; }\n\n.segmented-bar {\n  font-size: 13;\n  background-color: #303030;\n  color: #fff;\n  selected-background-color: #30bcff; }\n\n.sidedrawer-left, .sidedrawer-center {\n  background-color: #303030; }\n\n.sidedrawer-header {\n  background-color: #30bcff;\n  height: 148;\n  width: 100%; }\n\n.sidedrawer-left .sidedrawer-header {\n  padding: 16 16 0 16; }\n\n.sidedrawer-center .sidedrawer-header {\n  padding: 20 15 0 15; }\n\n.sidedrawer-header-image {\n  background-color: #e0e0e0; }\n\n.sidedrawer-left .sidedrawer-header-image {\n  height: 64;\n  width: 64;\n  border-radius: 32;\n  horizontal-align: left;\n  margin-bottom: 36; }\n\n.sidedrawer-center .sidedrawer-header-image {\n  height: 74;\n  width: 74;\n  border-radius: 37;\n  horizontal-align: center;\n  margin-bottom: 24; }\n\n.sidedrawer-header-brand {\n  color: #fff; }\n\n.sidedrawer-left .sidedrawer-header-brand {\n  horizontal-align: left;\n  font-size: 14; }\n\n.sidedrawer-center .sidedrawer-header-brand {\n  horizontal-align: center;\n  font-size: 15; }\n\n.sidedrawer-list-item {\n  height: 48;\n  horizontal-align: left;\n  width: 100%;\n  orientation: horizontal; }\n  .sidedrawer-list-item .sidedrawer-list-item-icon {\n    width: 24;\n    text-align: center;\n    font-size: 20;\n    height: 48;\n    vertical-align: center; }\n  .sidedrawer-list-item.active {\n    color: #30bcff;\n    background-color: #2b2b2b; }\n    .sidedrawer-list-item.active .sidedrawer-list-item-icon {\n      color: #fff; }\n\n.sidedrawer-left .sidedrawer-list-item-icon {\n  margin: 0 16 0 16; }\n\n.sidedrawer-center .sidedrawer-list-item-icon {\n  margin: 0 0 0 15; }\n\n.sidedrawer-list-item-text {\n  horizontal-align: left;\n  text-align: left;\n  font-size: 15;\n  background-color: transparent;\n  border-width: 0.1;\n  width: 80%;\n  vertical-align: center; }\n\n.sidedrawer-left .sidedrawer-list-item-text {\n  padding-left: 16; }\n\n.sidedrawer-center .sidedrawer-list-item-text {\n  padding-left: 15; }\n\n.slider {\n  background-color: #30bcff; }\n  .slider[isEnabled=false] {\n    background-color: #e0e0e0;\n    color: #e0e0e0; }\n\n.switch[checked=true] {\n  background-color: #30bcff; }\n\n.switch[checked=true][isEnabled=false] {\n  background-color: #e0e0e0;\n  color: #fff; }\n\n.switch[isEnabled=false] {\n  background-color: #e0e0e0;\n  color: #e0e0e0; }\n\n.tab-view {\n  /*color: $secondary;*/\n  selected-color: #30bcff;\n  tabs-background-color: #303030; }\n  .tab-view .tab-view-item {\n    background-color: #303030;\n    tabs-background-color: #303030; }\n\n#login-background {\n  margin-top: -20;\n  background-size: cover;\n  background-position: center; }\n\n.login-wrap {\n  padding: 0 40; }\n\n.logo-wrap {\n  margin: 60 0 10 0;\n  padding: 20 0; }\n  .logo-wrap .login-logo {\n    text-align: center;\n    font-size: 30;\n    font-weight: bold;\n    margin-bottom: 10;\n    opacity: 1;\n    color: #fff;\n    opacity: .9; }\n  .logo-wrap .login-logo-sub {\n    color: #fff;\n    opacity: .8;\n    text-align: center; }\n\n.login-wrapper {\n  padding: 20;\n  background-color: #fff;\n  border-radius: 3; }\n  .login-wrapper TextField {\n    padding: 10 10;\n    margin: 10 0 0 0; }\n\n.go-back {\n  font-size: 14;\n  text-align: center;\n  color: #fff;\n  margin-top: 10; }\n\n.icon {\n  font-family: \"Icomoon-font\", \"icomoon\", icomoon , Icomoon-font;\n  font-size: 25; }\n\nSwitch#userStatus[checked=true] {\n  color: #5094fc;\n  background-color: white;\n  transform: scale(1.25, 1.25);\n  transform: translate(-5, 0); }\n\nSwitch#userStatus[checked=false] {\n  color: #1750a5;\n  background-color: white; }\n\nPage {\n  color: #FFFFFF;\n  background-color: #707070;\n  font-size: 14; }\n\n.actionBar {\n  background-color: #707070;\n  color: #FFFFFF; }\n\nActionBar {\n  background-color: #707070;\n  color: #FFFFFF; }\n\nListView {\n  separator-color: transparent;\n  background-color: #262626; }\n\nStackLayout {\n  font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\n  font-size: 14;\n  padding: 5 0; }\n\nGridLayout {\n  text-align: center;\n  padding: 5; }\n\nSearchBar {\n  color: #707070;\n  background: #A8A8A8; }\n\nButton {\n  background-color: #1A75FE;\n  border-radius: 5;\n  height: 40;\n  color: #FFFFFF;\n  font-size: 15;\n  margin-top: 5; }\n\n.logo {\n  margin-bottom: 12;\n  height: 90;\n  font-weight: bold; }\n\n.form {\n  margin-left: 30;\n  margin-right: 30;\n  flex-grow: 2;\n  vertical-align: middle; }\n\n.page {\n  color: #FFFFFF;\n  background-color: #262626;\n  font-size: 20; }\n\n.pageModal {\n  color: #FFFFFF;\n  background-color: #707070; }\n\n.input {\n  text-align: center;\n  placeholder-color: #A8A8A8;\n  font-size: 15; }\n\n.input-field {\n  margin-bottom: 25; }\n\n.inputStyle {\n  border-bottom-width: none; }\n\n.hr-light {\n  background-color: #707070; }\n\n.login-label {\n  text-align: center;\n  margin-top: 7;\n  color: #A8A8A8; }\n\n.closeModal {\n  text-align: right;\n  font-weight: 800;\n  padding-top: 10; }\n\n.modalView {\n  width: 400;\n  text-align: center;\n  background-color: #262626;\n  color: #FFFFFF;\n  padding: 10 10; }\n\n.title {\n  font-size: 16; }\n\n.labelCalculadora {\n  font-size: 16;\n  text-align: center;\n  margin-bottom: 10; }\n\n.marginBotton {\n  margin-bottom: 10; }\n\n.marginTop {\n  margin-top: 10; }\n\n.strong {\n  font-weight: 800; }\n\n.medium {\n  font-weight: 500; }\n\n.right {\n  text-align: right; }\n\n.center {\n  text-align: center; }\n\n.left {\n  text-align: left; }\n\n.pads {\n  padding: 10 0 15 0; }\n\n.marginSides {\n  margin: 0 20; }\n\n.verticalCenter {\n  vertical-align: middle; }\n", ""]);

// exports
;
    if (false) {}


/***/ }),

/***/ "./components/Calculadora.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Calculadora_vue_vue_type_template_id_3a12e224___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Calculadora.vue?vue&type=template&id=3a12e224&");
/* harmony import */ var _Calculadora_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Calculadora.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Calculadora_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Calculadora_vue_vue_type_template_id_3a12e224___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Calculadora_vue_vue_type_template_id_3a12e224___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Calculadora.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Calculadora.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculadora_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Calculadora.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculadora_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Calculadora.vue?vue&type=template&id=3a12e224&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculadora_vue_vue_type_template_id_3a12e224___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Calculadora.vue?vue&type=template&id=3a12e224&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculadora_vue_vue_type_template_id_3a12e224___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculadora_vue_vue_type_template_id_3a12e224___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarExtra.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AdicionarExtra_vue_vue_type_template_id_22f3508e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=template&id=22f3508e&");
/* harmony import */ var _AdicionarExtra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AdicionarExtra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AdicionarExtra_vue_vue_type_template_id_22f3508e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AdicionarExtra_vue_vue_type_template_id_22f3508e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/AdicionarExtra.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarExtra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarExtra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=template&id=22f3508e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarExtra_vue_vue_type_template_id_22f3508e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarExtra.vue?vue&type=template&id=22f3508e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarExtra_vue_vue_type_template_id_22f3508e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarExtra_vue_vue_type_template_id_22f3508e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarMaterial.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AdicionarMaterial_vue_vue_type_template_id_a88ca4e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=template&id=a88ca4e4&");
/* harmony import */ var _AdicionarMaterial_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AdicionarMaterial_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AdicionarMaterial_vue_vue_type_template_id_a88ca4e4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AdicionarMaterial_vue_vue_type_template_id_a88ca4e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/AdicionarMaterial.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMaterial_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMaterial_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=template&id=a88ca4e4&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMaterial_vue_vue_type_template_id_a88ca4e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMaterial.vue?vue&type=template&id=a88ca4e4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMaterial_vue_vue_type_template_id_a88ca4e4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMaterial_vue_vue_type_template_id_a88ca4e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarMetragem.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AdicionarMetragem_vue_vue_type_template_id_3de7244b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=template&id=3de7244b&");
/* harmony import */ var _AdicionarMetragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AdicionarMetragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AdicionarMetragem_vue_vue_type_template_id_3de7244b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AdicionarMetragem_vue_vue_type_template_id_3de7244b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/AdicionarMetragem.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMetragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMetragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=template&id=3de7244b&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMetragem_vue_vue_type_template_id_3de7244b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/AdicionarMetragem.vue?vue&type=template&id=3de7244b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMetragem_vue_vue_type_template_id_3de7244b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdicionarMetragem_vue_vue_type_template_id_3de7244b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/Area.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Area_vue_vue_type_template_id_7bbc431c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Area.vue?vue&type=template&id=7bbc431c&");
/* harmony import */ var _Area_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Area.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Area_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Area_vue_vue_type_template_id_7bbc431c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Area_vue_vue_type_template_id_7bbc431c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/Area.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/Area.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Area_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Area.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Area_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/Area.vue?vue&type=template&id=7bbc431c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Area_vue_vue_type_template_id_7bbc431c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Area.vue?vue&type=template&id=7bbc431c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Area_vue_vue_type_template_id_7bbc431c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Area_vue_vue_type_template_id_7bbc431c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/Extras.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Extras_vue_vue_type_template_id_07c63bf0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Extras.vue?vue&type=template&id=07c63bf0&");
/* harmony import */ var _Extras_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Extras.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Extras_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Extras_vue_vue_type_template_id_07c63bf0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Extras_vue_vue_type_template_id_07c63bf0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/Extras.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/Extras.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Extras_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Extras.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Extras_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/Extras.vue?vue&type=template&id=07c63bf0&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Extras_vue_vue_type_template_id_07c63bf0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Extras.vue?vue&type=template&id=07c63bf0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Extras_vue_vue_type_template_id_07c63bf0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Extras_vue_vue_type_template_id_07c63bf0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/MaoDeObra.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MaoDeObra_vue_vue_type_template_id_06c30e89___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/MaoDeObra.vue?vue&type=template&id=06c30e89&");
/* harmony import */ var _MaoDeObra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/MaoDeObra.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MaoDeObra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MaoDeObra_vue_vue_type_template_id_06c30e89___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MaoDeObra_vue_vue_type_template_id_06c30e89___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/MaoDeObra.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/MaoDeObra.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_MaoDeObra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/MaoDeObra.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_MaoDeObra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/MaoDeObra.vue?vue&type=template&id=06c30e89&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MaoDeObra_vue_vue_type_template_id_06c30e89___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/MaoDeObra.vue?vue&type=template&id=06c30e89&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MaoDeObra_vue_vue_type_template_id_06c30e89___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MaoDeObra_vue_vue_type_template_id_06c30e89___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/Materiais.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Materiais_vue_vue_type_template_id_3dc6213a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Materiais.vue?vue&type=template&id=3dc6213a&");
/* harmony import */ var _Materiais_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Materiais.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Materiais_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Materiais_vue_vue_type_template_id_3dc6213a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Materiais_vue_vue_type_template_id_3dc6213a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/Materiais.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/Materiais.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Materiais_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Materiais.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Materiais_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/Materiais.vue?vue&type=template&id=3dc6213a&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Materiais_vue_vue_type_template_id_3dc6213a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Materiais.vue?vue&type=template&id=3dc6213a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Materiais_vue_vue_type_template_id_3dc6213a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Materiais_vue_vue_type_template_id_3dc6213a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/Metragem.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Metragem_vue_vue_type_template_id_7b715e49___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Metragem.vue?vue&type=template&id=7b715e49&");
/* harmony import */ var _Metragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Metragem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Metragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Metragem_vue_vue_type_template_id_7b715e49___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Metragem_vue_vue_type_template_id_7b715e49___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/Metragem.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/Metragem.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Metragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Metragem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Metragem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/Metragem.vue?vue&type=template&id=7b715e49&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Metragem_vue_vue_type_template_id_7b715e49___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Metragem.vue?vue&type=template&id=7b715e49&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Metragem_vue_vue_type_template_id_7b715e49___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Metragem_vue_vue_type_template_id_7b715e49___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/Selecionar.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Selecionar_vue_vue_type_template_id_e3d00e34___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Selecionar.vue?vue&type=template&id=e3d00e34&");
/* harmony import */ var _Selecionar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Selecionar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Selecionar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Selecionar_vue_vue_type_template_id_e3d00e34___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Selecionar_vue_vue_type_template_id_e3d00e34___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/Selecionar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/Selecionar.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Selecionar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Selecionar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Selecionar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/Selecionar.vue?vue&type=template&id=e3d00e34&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Selecionar_vue_vue_type_template_id_e3d00e34___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Selecionar.vue?vue&type=template&id=e3d00e34&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Selecionar_vue_vue_type_template_id_e3d00e34___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Selecionar_vue_vue_type_template_id_e3d00e34___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/Servicos.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Servicos_vue_vue_type_template_id_e220d50e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Servicos.vue?vue&type=template&id=e220d50e&");
/* harmony import */ var _Servicos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Servicos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Servicos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Servicos_vue_vue_type_template_id_e220d50e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Servicos_vue_vue_type_template_id_e220d50e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/Servicos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/Servicos.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Servicos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Servicos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Servicos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/Servicos.vue?vue&type=template&id=e220d50e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Servicos_vue_vue_type_template_id_e220d50e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Servicos.vue?vue&type=template&id=e220d50e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Servicos_vue_vue_type_template_id_e220d50e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Servicos_vue_vue_type_template_id_e220d50e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CalculadoraComponents/Superficies.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Superficies_vue_vue_type_template_id_e47e1d86___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CalculadoraComponents/Superficies.vue?vue&type=template&id=e47e1d86&");
/* harmony import */ var _Superficies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CalculadoraComponents/Superficies.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Superficies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Superficies_vue_vue_type_template_id_e47e1d86___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Superficies_vue_vue_type_template_id_e47e1d86___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CalculadoraComponents/Superficies.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CalculadoraComponents/Superficies.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Superficies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Superficies.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Superficies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CalculadoraComponents/Superficies.vue?vue&type=template&id=e47e1d86&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Superficies_vue_vue_type_template_id_e47e1d86___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CalculadoraComponents/Superficies.vue?vue&type=template&id=e47e1d86&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Superficies_vue_vue_type_template_id_e47e1d86___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Superficies_vue_vue_type_template_id_e47e1d86___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Login.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_c27482c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Login.vue?vue&type=template&id=c27482c4&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_c27482c4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_c27482c4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Login.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Login.vue?vue&type=template&id=c27482c4&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_c27482c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Login.vue?vue&type=template&id=c27482c4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_c27482c4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_c27482c4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Pedidos.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Pedidos_vue_vue_type_template_id_6dda1343___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Pedidos.vue?vue&type=template&id=6dda1343&");
/* harmony import */ var _Pedidos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Pedidos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Pedidos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Pedidos_vue_vue_type_template_id_6dda1343___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Pedidos_vue_vue_type_template_id_6dda1343___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Pedidos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Pedidos.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Pedidos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Pedidos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Pedidos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Pedidos.vue?vue&type=template&id=6dda1343&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Pedidos_vue_vue_type_template_id_6dda1343___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Pedidos.vue?vue&type=template&id=6dda1343&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Pedidos_vue_vue_type_template_id_6dda1343___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Pedidos_vue_vue_type_template_id_6dda1343___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/ResetSenha.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ResetSenha_vue_vue_type_template_id_1105423e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/ResetSenha.vue?vue&type=template&id=1105423e&");
/* harmony import */ var _ResetSenha_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/ResetSenha.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ResetSenha_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ResetSenha_vue_vue_type_template_id_1105423e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ResetSenha_vue_vue_type_template_id_1105423e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/ResetSenha.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/ResetSenha.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetSenha_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/ResetSenha.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetSenha_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/ResetSenha.vue?vue&type=template&id=1105423e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetSenha_vue_vue_type_template_id_1105423e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/ResetSenha.vue?vue&type=template&id=1105423e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetSenha_vue_vue_type_template_id_1105423e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetSenha_vue_vue_type_template_id_1105423e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Resumo.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Resumo_vue_vue_type_template_id_f7d49c5c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Resumo.vue?vue&type=template&id=f7d49c5c&");
/* harmony import */ var _Resumo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Resumo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Resumo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Resumo_vue_vue_type_template_id_f7d49c5c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Resumo_vue_vue_type_template_id_f7d49c5c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Resumo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Resumo.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Resumo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Resumo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Resumo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Resumo.vue?vue&type=template&id=f7d49c5c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Resumo_vue_vue_type_template_id_f7d49c5c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Resumo.vue?vue&type=template&id=f7d49c5c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Resumo_vue_vue_type_template_id_f7d49c5c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Resumo_vue_vue_type_template_id_f7d49c5c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/shared/ActionBar.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ActionBar_vue_vue_type_template_id_2618786e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/shared/ActionBar.vue?vue&type=template&id=2618786e&");
/* harmony import */ var _ActionBar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/shared/ActionBar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ActionBar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ActionBar_vue_vue_type_template_id_2618786e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ActionBar_vue_vue_type_template_id_2618786e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/shared/ActionBar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/shared/ActionBar.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionBar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/shared/ActionBar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionBar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/shared/ActionBar.vue?vue&type=template&id=2618786e&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionBar_vue_vue_type_template_id_2618786e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/shared/ActionBar.vue?vue&type=template&id=2618786e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionBar_vue_vue_type_template_id_2618786e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionBar_vue_vue_type_template_id_2618786e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./main.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var nativescript_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-vue/dist/index.js");
/* harmony import */ var nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nativescript_vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Login__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Login.vue");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./store.js");

        let applicationCheckPlatform = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
        if (applicationCheckPlatform.android && !global["__snapshot"]) {
            __webpack_require__("../node_modules/tns-core-modules/ui/frame/frame.js");
__webpack_require__("../node_modules/tns-core-modules/ui/frame/activity.js");
        }

        
            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-regular.js")();
            
            
        if (false) {}
        
            const context = __webpack_require__("./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$");
            global.registerWebpackModules(context);
            
        __webpack_require__("../node_modules/tns-core-modules/bundle-entry-points.js");
        

 // Prints Vue logs when --env.production is *NOT* set while building
//Vue.config.silent = (TNS_ENV === 'production')

new nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default.a({
  store: _store__WEBPACK_IMPORTED_MODULE_2__["default"],
  render: h => h('frame', [h(_components_Login__WEBPACK_IMPORTED_MODULE_1__["default"])])
}).$start();
    
        
        
    
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/nativescript-dev-webpack/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"android":{"v8Flags":"--expose_gc"},"main":"main","name":"sppinturas","version":"1.0.0"};

/***/ }),

/***/ "./store.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-vue/dist/index.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/http/http.js");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vuex__WEBPACK_IMPORTED_MODULE_1__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (new vuex__WEBPACK_IMPORTED_MODULE_1__["default"].Store({
  state: {
    pedidos: [],
    configuracao: {
      valor_metro_quadrado: 0,
      numero_de_andares: 0,
      numero_de_predios: 0,
      areaExterna_fator_multiplicador: 0,
      areaInterna_fator_multiplicador: 0,
      areaExtra_fator_multiplicador: 0,
      superficies: [],
      lista_servicos: [],
      lista_area_extra: [],
      lista_materiais: []
    },
    pedido: {
      id_pedido: 1,
      valor_total: 0,
      orcamento: {
        orcamento_area_externa: {
          valor_total: 0,
          detalhes_valor: {
            valor_pintura: 0,
            valor_servicos: 0
          },
          metragem: 0,
          superficie: "",
          servicos: []
        },
        orcamento_area_interna: {
          valor_total: 0,
          detalhes_valor: {
            valor_pintura: 0,
            valor_servicos: 0
          },
          metragem: 0,
          superficie: "",
          servicos: []
        },
        orcamento_area_extra: {
          valor_total: 0,
          areas: []
        },
        materiais: []
      }
    },
    areaExtraTemp: {
      isEditExtra: false,
      isExtra: false,
      nome: "",
      valor_total: 0,
      detalhes_valor: {
        valor_pintura: 0,
        valor_servicos: 0
      },
      metragem: 0,
      superficie: "",
      servicos: []
    },
    areaInternaConfig: {
      isEditInterna: false,
      isInterna: false
    }
  },
  getters: {
    orcamento_valor_total: state => {
      let list = [];
      state.pedido.orcamento.orcamento_area_extra.areas.forEach(el => {
        list.push(el.valor_total);
      });
      return parseFloat((state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura + parseFloat(state.pedido.orcamento.orcamento_area_externa.servicos.map(function (e) {
        return e.valor;
      }).reduce((sum, a) => sum + a, 0).toFixed(2)) + state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura + parseFloat(state.pedido.orcamento.orcamento_area_interna.servicos.map(function (e) {
        return e.valor;
      }).reduce((sum, a) => sum + a, 0).toFixed(2)) + parseFloat(list.reduce((sum, a) => sum + a, 0).toFixed(2))).toFixed(2));
    },
    /////// Configurações ///////////////////////////////////////////////////
    setar_superficies: state => {
      return state.configuracao.superficies;
    },
    setar_servicos: state => {
      return state.configuracao.lista_servicos;
    },
    setar_areas_extras: state => {
      return state.configuracao.lista_area_extra;
    },
    setar_materiais: state => {
      return state.configuracao.lista_materiais;
    },
    /////// Area Externa ///////////////////////////////////////////////////
    orcamento_area_externa_valor: state => {
      let valor_pintura_externa = state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura;
      let valor_servicos_externa = state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos;
      return parseFloat((valor_pintura_externa + valor_servicos_externa).toFixed(2));
    },
    orcamento_area_externa_detalhes_valor_pintura: state => {
      return state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura;
    },
    orcamento_area_externa_detalhes_valor_servicos: state => {
      return parseFloat(state.pedido.orcamento.orcamento_area_externa.servicos.map(function (e) {
        return e.valor;
      }).reduce((sum, a) => sum + a, 0).toFixed(2));
    },
    orcamento_area_externa_servicos: state => {
      return state.pedido.orcamento.orcamento_area_externa.servicos;
    },
    /////// Area Interna /////////////////////////////////////////////////// 
    orcamento_area_interna_valor: state => {
      let valor_pintura_interna = state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura;
      let valor_servicos_interna = state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos;
      return parseFloat((valor_pintura_interna + valor_servicos_interna).toFixed(2));
    },
    orcamento_area_interna_detalhes_valor_pintura: state => {
      return state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura;
    },
    orcamento_area_interna_detalhes_valor_servicos: state => {
      return parseFloat(state.pedido.orcamento.orcamento_area_interna.servicos.map(function (e) {
        return e.valor;
      }).reduce((sum, a) => sum + a, 0).toFixed(2));
    },
    orcamento_area_interna_servicos: state => {
      return state.pedido.orcamento.orcamento_area_interna.servicos;
    },
    /////// Area Extras ///////////////////////////////////////////////////
    orcamento_area_extras_areas: state => {
      return state.pedido.orcamento.orcamento_area_extra.areas;
    },
    orcamento_area_extras_valor_total: state => {
      let list = [];
      state.pedido.orcamento.orcamento_area_extra.areas.forEach(el => {
        list.push(el.valor_total);
      });
      return parseFloat(list.reduce((sum, a) => sum + a, 0).toFixed(2));
    },
    ////// TEMPORARY ADD EXTRA///////////////////////////////////////////////////// 
    orcamento_area_extra_valor: state => {
      let valor_pintura_extra = state.areaExtraTemp.detalhes_valor.valor_pintura;
      let valor_servicos_extra = state.areaExtraTemp.detalhes_valor.valor_servicos;
      return parseFloat((valor_pintura_extra + valor_servicos_extra).toFixed(2));
    },
    orcamento_area_extra_modal_valor: state => {
      return state.areaExtraTemp.valor_total;
    },
    orcamento_area_extra_modal_nome: state => {
      return state.areaExtraTemp.nome;
    },
    orcamento_area_extra_modal_isEdit: state => {
      return state.areaExtraTemp.isEditExtra;
    },
    orcamento_area_extra_servicos: state => {
      return state.areaExtraTemp.servicos;
    },
    orcamento_area_extra_detalhes_valor_pintura: state => {
      return state.areaExtraTemp.detalhes_valor.valor_pintura;
    },
    orcamento_area_extra_detalhes_valor_servicos: state => {
      return parseFloat(state.areaExtraTemp.servicos.map(function (e) {
        return e.valor;
      }).reduce((sum, a) => sum + a, 0).toFixed(2));
    },
    orcamento_area_extra_is_extra: state => {
      return state.areaExtraTemp.isExtra;
    },
    ////// TEMPORARY ADD Interna///////////////////////////////////////////////////// 
    orcamento_area_interna_is_interna: state => {
      return state.areaInternaConfig.isInterna;
    },
    ////// MATERIAIS ///////
    set_materiais: state => {
      return state.pedido.orcamento.materiais;
    }
  },
  mutations: {
    /////// Conf ///////////////////////////////////////////////////
    AREA_EXTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR: (state, payload) => {
      state.configuracao.areaExterna_fator_multiplicador = payload;
    },
    AREA_INTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR: (state, payload) => {
      state.configuracao.areaInterna_fator_multiplicador = payload;
    },
    AREA_EXTRA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR: (state, payload) => {
      state.configuracao.areaExtra_fator_multiplicador = payload;
    },
    RECEBER_PEDIDOS: state => {
      http__WEBPACK_IMPORTED_MODULE_2__["getJSON"]("https://sppinturas.herokuapp.com/pedidos").then(result => {
        state.pedidos = result.pedidos;
      }, error => {
        console.log(error);
      });
    },
    ENVIAR_ORCAMENTO: state => {
      state.pedido.valor_total = state.pedido.orcamento.orcamento_area_externa.valor_total + state.pedido.orcamento.orcamento_area_interna.valor_total + state.pedido.orcamento.orcamento_area_extra.valor_total;
    },
    RECEBER_SUPERFICIES: state => {
      http__WEBPACK_IMPORTED_MODULE_2__["getJSON"]("https://sppinturas.herokuapp.com/superficies_disponiveis").then(result => {
        state.configuracao.superficies = result.superficies_disponiveis;
      }, error => {
        console.log(error);
      });
    },
    RECEBER_SERVICOS: state => {
      let list = [];
      http__WEBPACK_IMPORTED_MODULE_2__["getJSON"]("https://sppinturas.herokuapp.com/servicos_disponiveis").then(function (result) {
        result.servicos_disponiveis.forEach(el => {
          list.push(el.descricao);
        });
        state.configuracao.lista_servicos = list;
      }, error => {
        console.log(error);
      });
    },
    RECEBER_AREAS_EXTRAS: state => {
      let list = [];
      http__WEBPACK_IMPORTED_MODULE_2__["getJSON"]("https://sppinturas.herokuapp.com/areas_extras_disponiveis").then(function (result) {
        result.areas_extras_disponiveis.forEach(el => {
          list.push(el.nome);
        });
        state.configuracao.lista_area_extra = list;
      }, error => {
        console.log(error);
      });
    },
    RECEBER_MATERIAIS: state => {
      let list = [];
      http__WEBPACK_IMPORTED_MODULE_2__["getJSON"]("https://sppinturas.herokuapp.com/materiais_disponiveis").then(function (result) {
        result.materiais_disponiveis.forEach(el => {
          list.push(el.descricao);
        });
        state.configuracao.lista_materiais = list;
      }, error => {
        console.log(error);
      });
    },
    SETAR_CONFIGURACOES_PEDIDO: (state, payload) => {
      state.configuracao.numero_de_andares = payload.numero_de_andares;
      state.configuracao.numero_de_predios = payload.numero_de_predios;
      state.pedido.id_pedido = payload.id_pedido;
    },
    SETAR_VALOR_METRO: state => {
      http__WEBPACK_IMPORTED_MODULE_2__["getJSON"]("https://sppinturas.herokuapp.com/valor_do_metro_quadrado").then(function (result) {
        state.configuracao.valor_metro_quadrado = result.valor_do_metro_quadrado.valor;
      }, error => {
        console.log(error);
      });
    },
    ZERAR_PEDIDO: state => {
      state.pedido = {
        id_pedido: 1,
        valor_total: 0,
        orcamento: {
          orcamento_area_externa: {
            valor_total: 0,
            detalhes_valor: {
              valor_pintura: 0,
              valor_servicos: 0
            },
            metragem: 0,
            superficie: "",
            servicos: []
          },
          orcamento_area_interna: {
            valor_total: 0,
            detalhes_valor: {
              valor_pintura: 0,
              valor_servicos: 0
            },
            metragem: 0,
            superficie: "",
            servicos: []
          },
          orcamento_area_extra: {
            valor_total: 0,
            areas: []
          },
          materiais: []
        }
      };
    },
    /////// Area Externa/////////////////////////////////////////////
    AREA_EXTERNA_SETAR_VALOR_TOTAL: state => {
      state.pedido.orcamento.orcamento_area_externa.valor_total = parseFloat((state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura + state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos).toFixed(2));
    },
    // Detalhes
    AREA_EXTERNA_SETAR_DETALHES_VALOR_PINTURA: (state, payload) => {
      state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura = parseFloat((payload * state.configuracao.valor_metro_quadrado * state.configuracao.numero_de_andares * state.configuracao.numero_de_predios * state.configuracao.areaExterna_fator_multiplicador).toFixed(2));
    },
    AREA_EXTERNA_RECALCULAR_DETALHES_VALOR_PINTURA: state => {
      state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura = parseFloat((state.pedido.orcamento.orcamento_area_externa.metragem * state.configuracao.valor_metro_quadrado * state.configuracao.numero_de_andares * state.configuracao.numero_de_predios * state.configuracao.areaExterna_fator_multiplicador).toFixed(2));
    },
    AREA_EXTERNA_RECALCULAR_VALOR_SERVICOS: state => {
      state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos = parseFloat(state.pedido.orcamento.orcamento_area_externa.servicos.map(function (e) {
        return e.valor;
      }).reduce((sum, a) => sum + a, 0).toFixed(2));
    },
    AREA_EXTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO: (state, payload) => {
      let valorAtual = parseFloat(state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos.toFixed(2));
      let valorEntrada = parseFloat(payload.toFixed(2));
      let total = parseFloat((valorAtual + valorEntrada).toFixed(2));
      state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos = total;
    },
    AREA_EXTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO: (state, payload) => {
      let valorAtual = parseFloat(state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos.toFixed(2));
      let valorEntrada = parseFloat(payload.toFixed(2));
      let total = parseFloat(valorAtual - valorEntrada.toFixed(2));
      state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_servicos = total;
    },
    AREA_EXTERNA_ZERAR_DETALHES_VALOR_PINTURA: (state, payload) => {
      state.pedido.orcamento.orcamento_area_externa.detalhes_valor.valor_pintura = 0;
    },
    AREA_EXTERNA_SETAR_SUPERFICIE: (state, payload) => {
      state.pedido.orcamento.orcamento_area_externa.superficie = payload;
    },
    //Metragem
    AREA_EXTERNA_METRAGEM: (state, payload) => {
      state.pedido.orcamento.orcamento_area_externa.metragem = payload;
    },
    //Servicos
    AREA_EXTERNA_ADICIONAR_SERVICOS: (state, payload) => {
      state.pedido.orcamento.orcamento_area_externa.servicos.push(payload);
    },
    AREA_EXTERNA_REMOVER_SERVICOS: (state, payload) => {
      state.pedido.orcamento.orcamento_area_externa.servicos.splice(payload, 1);
    },
    AREA_EXTERNA_SALVAR_SERVICOS: (state, payload) => {
      state.pedido.orcamento.orcamento_area_externa.servicos.splice(payload.index, 1, payload.element);
    },
    /////// Area Interna ///////////////////////////////////////////////////
    AREA_INTERNA_SETAR_VALOR_TOTAL: state => {
      state.pedido.orcamento.orcamento_area_interna.valor_total = parseFloat((state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura + state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos).toFixed(2));
    },
    // Detalhes
    AREA_INTERNA_SETAR_DETALHES_VALOR_PINTURA: (state, payload) => {
      state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura = parseFloat((payload * state.configuracao.valor_metro_quadrado * state.configuracao.numero_de_andares * state.configuracao.numero_de_predios * state.configuracao.areaInterna_fator_multiplicador).toFixed(2));
    },
    AREA_INTERNA_RECALCULAR_DETALHES_VALOR_PINTURA: state => {
      state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura = parseFloat((state.pedido.orcamento.orcamento_area_interna.metragem * state.configuracao.valor_metro_quadrado * state.configuracao.numero_de_andares * state.configuracao.numero_de_predios * state.configuracao.areaInterna_fator_multiplicador).toFixed(2));
    },
    AREA_INTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO: (state, payload) => {
      let valorAtual = parseFloat(state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos.toFixed(2));
      let valorEntrada = parseFloat(payload.toFixed(2));
      let total = parseFloat((valorAtual + valorEntrada).toFixed(2));
      state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos = total;
    },
    AREA_INTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO: (state, payload) => {
      let valorAtual = parseFloat(state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos.toFixed(2));
      let valorEntrada = parseFloat(payload.toFixed(2));
      let total = parseFloat(valorAtual - valorEntrada.toFixed(2));
      state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos = total;
    },
    AREA_INTERNA_ZERAR_DETALHES_VALOR_PINTURA: (state, payload) => {
      state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_pintura = 0;
    },
    AREA_INTERNA_SETAR_SUPERFICIE: (state, payload) => {
      state.pedido.orcamento.orcamento_area_interna.superficie = payload;
    },
    //Metragem
    AREA_INTERNA_METRAGEM: (state, payload) => {
      state.pedido.orcamento.orcamento_area_interna.metragem = payload;
    },
    //Servicos
    AREA_INTERNA_ADICIONAR_SERVICOS: (state, payload) => {
      state.pedido.orcamento.orcamento_area_interna.servicos.push(payload);
    },
    AREA_INTERNA_REMOVER_SERVICOS: (state, payload) => {
      state.pedido.orcamento.orcamento_area_interna.servicos.splice(payload, 1);
    },
    AREA_INTERNA_SALVAR_SERVICOS: (state, payload) => {
      state.pedido.orcamento.orcamento_area_interna.servicos.splice(payload.index, 1, payload.element);
    },
    AREA_INTERNA_RECALCULAR_VALOR_SERVICOS: state => {
      state.pedido.orcamento.orcamento_area_interna.detalhes_valor.valor_servicos = parseFloat(state.pedido.orcamento.orcamento_area_interna.servicos.map(function (e) {
        return e.valor;
      }).reduce((sum, a) => sum + a, 0).toFixed(2));
    },
    /////// Area Extra ///////////////////////////////////////////////////
    AREAS_EXTRAS_CALCULAR_VALOR_TOTAL: (state, payload) => {
      state.pedido.orcamento.orcamento_area_extra.valor_total = payload;
    },
    AREA_EXTRA_ADICIONAR_AREA: (state, payload) => {
      state.pedido.orcamento.orcamento_area_extra.areas.push(payload);
    },
    AREA_EXTRA_SALVAR_AREA: (state, payload) => {
      state.pedido.orcamento.orcamento_area_extra.areas.splice(payload.index, 1, payload.element);
    },
    /////// Area Extra MODAL ///////////////////////////////////////////////////
    AREA_EXTRA_SETAR_NOME: (state, payload) => {
      state.areaExtraTemp.nome = payload;
    },
    AREA_EXTRA_ZERAR_NOME: state => {
      state.areaExtraTemp.nome = '';
    },
    AREA_EXTRA_METRAGEM: (state, payload) => {
      state.areaExtraTemp.metragem = payload;
    },
    AREA_EXTRA_ZERAR_DETALHES_VALOR_PINTURA: state => {
      state.areaExtraTemp.detalhes_valor.valor_pintura = 0;
    },
    AREA_EXTRA_RECALCULAR_DETALHES_VALOR_PINTURA: state => {
      state.areaExtraTemp.detalhes_valor.valor_pintura = parseFloat((state.areaExtraTemp.metragem * state.configuracao.valor_metro_quadrado * state.configuracao.numero_de_andares * state.configuracao.numero_de_predios * state.configuracao.areaExtra_fator_multiplicador).toFixed(2));
    },
    AREA_EXTRA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO: (state, payload) => {
      let valorAtual = parseFloat(state.areaExtraTemp.detalhes_valor.valor_servicos.toFixed(2));
      let valorEntrada = parseFloat(payload.toFixed(2));
      let total = parseFloat((valorAtual + valorEntrada).toFixed(2));
      state.areaExtraTemp.detalhes_valor.valor_servicos = total;
    },
    AREA_EXTRA_IS_EXTRA: state => {
      state.areaExtraTemp.isExtra = true;
    },
    AREA_EXTRA_ISNT_EXTRA: state => {
      state.areaExtraTemp.isExtra = false;
    },
    AREA_EXTRA_ADICIONAR_SERVICOS: (state, payload) => {
      state.areaExtraTemp.servicos.push(payload);
    },
    AREA_EXTRA_REMOVER_SERVICOS: (state, payload) => {
      state.areaExtraTemp.servicos.splice(payload, 1);
    },
    AREA_EXTRA_SALVAR_SERVICOS: (state, payload) => {
      state.areaExtraTemp.servicos.splice(payload.index, 1, payload.element);
    },
    AREA_EXTRA_RESETAR: state => state.areaExtraTemp = {
      isEditExtra: false,
      isExtra: false,
      nome: "",
      valor_total: 0,
      detalhes_valor: {
        valor_pintura: 0,
        valor_servicos: 0
      },
      metragem: 0,
      superficie: "",
      servicos: []
    },
    AREA_EXTRA_ZERAR_SUPERFICIE: state => {
      state.configuracao.areaExtra_fator_multiplicador = 0;
    },
    AREA_EXTRA_SETAR_SUPERFICIE: (state, payload) => {
      state.areaExtraTemp.superficie = payload;
    },
    AREA_EXTRA_SETAR_VALOR_TOTAL: (state, payload) => {
      state.areaExtraTemp.valor_total = payload;
    },
    AREA_EXTRA_SETAR_MODAL: (state, payload) => {
      state.areaExtraTemp = payload;
    },
    AREA_EXTRA_SALVAR_MODAL: (state, payload) => {
      state.pedido.orcamento.orcamento_area_extra.areas.splice(payload.index, 1, payload.element);
    },
    AREA_EXTRA_EXCLUIR_MODAL: (state, payload) => {
      state.pedido.orcamento.orcamento_area_extra.areas.splice(payload, 1);
    },
    //////// Materiais ////////
    MATERIAIS_ADICIONAR: (state, payload) => {
      state.pedido.orcamento.materiais.push(payload);
    },
    MATERIAS_REMOVER: (state, payload) => {
      state.pedido.orcamento.materiais.splice(payload, 1);
    },
    MATERIAS_SALVAR: (state, payload) => {
      state.pedido.orcamento.materiais.splice(payload.index, 1, payload.element);
    }
  },
  actions: {
    // Config
    AreaExterna_setar_fator_multiplicador(context, payload) {
      context.commit('AREA_EXTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR', payload);
    },

    AreaInterna_setar_fator_multiplicador(context, payload) {
      context.commit('AREA_INTERNA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR', payload);
    },

    AreaExtra_setar_fator_multiplicador(context, payload) {
      context.commit('AREA_EXTRA_CONFIGURACAO_SETAR_FATOR_MULTIPLICADOR', payload);
    },

    receber_superficies(context) {
      context.commit('RECEBER_SUPERFICIES');
    },

    receber_servicos(context) {
      context.commit('RECEBER_SERVICOS');
    },

    receber_areas_extras(context) {
      context.commit('RECEBER_AREAS_EXTRAS');
    },

    receber_materiais(context) {
      context.commit('RECEBER_MATERIAIS');
    },

    receber_configuracoes_pedido(context, payload) {
      context.commit('SETAR_CONFIGURACOES_PEDIDO', payload);
    },

    receber_valor_metro(context) {
      context.commit('SETAR_VALOR_METRO');
    },

    ///////  Pedidos ////////
    receber_pedidos(context) {
      context.commit('RECEBER_PEDIDOS');
    },

    zerar_pedidos(context) {
      context.commit('ZERAR_PEDIDO');
    },

    /////// Area Externa ///////////////////////////////////////////////////
    AreaExterna_setar_valor_total(context) {
      context.commit('AREA_EXTERNA_SETAR_VALOR_TOTAL');
    },

    AreaExterna_recalcular_detalhes_valor_pintura(context) {
      context.commit('AREA_EXTERNA_RECALCULAR_DETALHES_VALOR_PINTURA');
    },

    AreaExterna_setar_metragem(context, payload) {
      context.commit('AREA_EXTERNA_METRAGEM', payload);
    },

    AreaExterna_setar_detalhes_valor_pintura(context, payload) {
      context.commit('AREA_EXTERNA_SETAR_DETALHES_VALOR_PINTURA', payload);
    },

    AreaExterna_recalcular_valor_servico(context) {
      context.commit('AREA_EXTERNA_RECALCULAR_VALOR_SERVICOS');
    },

    AreaExterna_adicionar_servico(context, payload) {
      context.commit('AREA_EXTERNA_ADICIONAR_SERVICOS', payload);
    },

    AreaExterna_remover_servico(context, payload) {
      context.commit('AREA_EXTERNA_REMOVER_SERVICOS', payload);
    },

    AreaExterna_salvar_servico(context, payload) {
      context.commit('AREA_EXTERNA_SALVAR_SERVICOS', payload);
    },

    AreaExterna_adicionar_valor_servicos(context, payload) {
      context.commit('AREA_EXTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload);
    },

    AreaExterna_remover_valor_servicos(context, payload) {
      context.commit('AREA_EXTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO', payload);
    },

    AreaExterna_zerar_valor_pintura(context, payload) {
      context.commit('AREA_EXTERNA_ZERAR_DETALHES_VALOR_PINTURA', payload);
    },

    AreaExterna_setar_superficie(context, payload) {
      context.commit('AREA_EXTERNA_SETAR_SUPERFICIE', payload);
    },

    /////// Area Interna ///////////////////////////////////////////////////
    AreaInterna_setar_valor_total(context) {
      context.commit('AREA_INTERNA_SETAR_VALOR_TOTAL');
    },

    AreaInterna_recalcular_detalhes_valor_pintura(context) {
      context.commit('AREA_INTERNA_RECALCULAR_DETALHES_VALOR_PINTURA');
    },

    AreaInterna_setar_metragem(context, payload) {
      context.commit('AREA_INTERNA_METRAGEM', payload);
    },

    AreaInterna_setar_detalhes_valor_pintura(context, payload) {
      context.commit('AREA_INTERNA_SETAR_DETALHES_VALOR_PINTURA', payload);
    },

    AreaInterna_adicionar_servico(context, payload) {
      context.commit('AREA_INTERNA_ADICIONAR_SERVICOS', payload);
    },

    AreaInterna_remover_servico(context, payload) {
      context.commit('AREA_INTERNA_REMOVER_SERVICOS', payload);
    },

    AreaInterna_salvar_servico(context, payload) {
      context.commit('AREA_INTERNA_SALVAR_SERVICOS', payload);
    },

    AreaInterna_adicionar_valor_servicos(context, payload) {
      context.commit('AREA_INTERNA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload);
    },

    AreaInterna_remover_valor_servicos(context, payload) {
      context.commit('AREA_INTERNA_RECALCULAR_DETALHES_REMOVER_VALOR_SERVICO', payload);
    },

    AreaInterna_zerar_valor_pintura(context, payload) {
      context.commit('AREA_INTERNA_ZERAR_DETALHES_VALOR_PINTURA', payload);
    },

    AreaInterna_setar_superficie(context, payload) {
      context.commit('AREA_INTERNA_SETAR_SUPERFICIE', payload);
    },

    AreaInterna_recalcular_valor_servicos(context) {
      context.commit('AREA_INTERNA_RECALCULAR_VALOR_SERVICOS');
    },

    /////// Area Extra ///////////////////////////////////////////////////
    AreaExtra_adicionar_area(context, payload) {
      context.commit('AREA_EXTRA_ADICIONAR_AREA', payload);
    },

    AreaExtra_salvar_area(context, payload) {
      context.commit('AREA_EXTRA_SALVAR_AREA', payload);
    },

    /////// Area Extra Modal ///////////////////////////////////////////////////
    AreaExtra_recalcular_detalhes_valor_pintura(context) {
      context.commit('AREA_EXTRA_RECALCULAR_DETALHES_VALOR_PINTURA');
    },

    AreaExtra_setar_nome(context, payload) {
      context.commit('AREA_EXTRA_SETAR_NOME', payload);
    },

    AreaExtra_zerar_nome(context) {
      context.commit('AREA_EXTRA_ZERAR_NOME');
    },

    AreaExtra_setar_metragem(context, payload) {
      context.commit('AREA_EXTRA_METRAGEM', payload);
    },

    AreaExtra_zerar_valor_pintura(context) {
      context.commit('AREA_EXTRA_ZERAR_DETALHES_VALOR_PINTURA');
    },

    AreaExtra_is_extra(context) {
      context.commit('AREA_EXTRA_IS_EXTRA');
    },

    AreaExtra_isnt_extra(context) {
      context.commit('AREA_EXTRA_ISNT_EXTRA');
    },

    AreaExtra_adicionar_servico(context, payload) {
      context.commit('AREA_EXTRA_ADICIONAR_SERVICOS', payload);
    },

    AreaExtra_adicionar_valor_servico(context, payload) {
      context.commit('AREA_EXTRA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload);
    },

    AreaExtra_remover_servico(context, payload) {
      context.commit('AREA_EXTRA_REMOVER_SERVICOS', payload);
    },

    AreaExtra_salvar_servico(context, payload) {
      context.commit('AREA_EXTRA_SALVAR_SERVICOS', payload);
    },

    AreaExtra_resetar(context) {
      context.commit('AREA_EXTRA_RESETAR');
    },

    AreaExtra_setar_superficie(context, payload) {
      context.commit('AREA_EXTRA_SETAR_SUPERFICIE', payload);
    },

    AreaExtra_zerar_superficie(context) {
      context.commit('AREA_EXTRA_ZERAR_SUPERFICIE');
    },

    AreaExtra_setar_valor_total(context, payload) {
      context.commit('AREA_EXTRA_SETAR_VALOR_TOTAL', payload);
    },

    AreaExtra_setar_area(context, payload) {
      context.commit('AREA_EXTRA_SETAR_MODAL', payload);
    },

    AreaExtra_salvar_area(context, payload) {
      context.commit('AREA_EXTRA_SALVAR_MODAL', payload);
    },

    AreaExtra_excluir_area(context, payload) {
      context.commit('AREA_EXTRA_EXCLUIR_MODAL', payload);
    },

    ///////  Materiais  //////    
    materiais_adicionar(context, payload) {
      context.commit('MATERIAIS_ADICIONAR', payload);
    },

    materiais_remover(context, payload) {
      context.commit('MATERIAS_REMOVER', payload);
    },

    materiais_salvar(context, payload) {
      context.commit('MATERIAS_SALVAR', payload);
    },

    //// Area Extra 
    AreaExtra_calcular_valor_total(context, payload) {
      context.commit('AREAS_EXTRAS_CALCULAR_VALOR_TOTAL', payload);
    },

    //// Area Interna Temp
    AreaInterna_recalcular_detalhes_valor_pintura(context) {
      context.commit('AREA_INTERNA_RECALCULAR_DETALHES_VALOR_PINTURA');
    },

    AreaExtra_setar_nome(context, payload) {
      context.commit('AREA_EXTRA_SETAR_NOME', payload);
    },

    AreaExtra_zerar_nome(context) {
      context.commit('AREA_EXTRA_ZERAR_NOME');
    },

    AreaExtra_setar_metragem(context, payload) {
      context.commit('AREA_EXTRA_METRAGEM', payload);
    },

    AreaExtra_zerar_valor_pintura(context) {
      context.commit('AREA_EXTRA_ZERAR_DETALHES_VALOR_PINTURA');
    },

    AreaExtra_is_extra(context) {
      context.commit('AREA_EXTRA_IS_EXTRA');
    },

    AreaExtra_isnt_extra(context) {
      context.commit('AREA_EXTRA_ISNT_EXTRA');
    },

    AreaExtra_adicionar_servico(context, payload) {
      context.commit('AREA_EXTRA_ADICIONAR_SERVICOS', payload);
    },

    AreaExtra_adicionar_valor_servico(context, payload) {
      context.commit('AREA_EXTRA_RECALCULAR_DETALHES_ADICIONAR_VALOR_SERVICO', payload);
    },

    AreaExtra_remover_servico(context, payload) {
      context.commit('AREA_EXTRA_REMOVER_SERVICOS', payload);
    },

    AreaExtra_salvar_servico(context, payload) {
      context.commit('AREA_EXTRA_SALVAR_SERVICOS', payload);
    },

    AreaExtra_resetar(context) {
      context.commit('AREA_EXTRA_RESETAR');
    },

    AreaExtra_setar_superficie(context, payload) {
      context.commit('AREA_EXTRA_SETAR_SUPERFICIE', payload);
    },

    AreaExtra_zerar_superficie(context) {
      context.commit('AREA_EXTRA_ZERAR_SUPERFICIE');
    },

    SetarValorTotal(context) {
      context.commit('ENVIAR_ORCAMENTO');
    }

  }
}));

/***/ })

/******/ });